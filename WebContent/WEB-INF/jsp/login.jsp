<%@page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>

<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/jquery.js"></script>

<title>Iniciar sesió - Naturadventure</title>
<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/assets/css/style-login.css" rel="stylesheet">

	<!-- Evitem que el usuari accedixca a esta pàgina si ja ha iniciat sessió
	http://stackoverflow.com/a/5411567 -->
	<c:if test="${not empty sessionScope.personaRegistrada}">
	  <meta http-equiv="refresh" content="0; url=${pageContext.request.contextPath}/index.html" />
	</c:if>
</head>

<body>

	<div class="container">
		<h2>Iniciar sessió</h2>
		<form:form method="post" modelAttribute="user"
			action="${pageContext.request.contextPath}/login.html">
			<div class="form-group">
				<form:label path="username">Usuari:</form:label>
				<form:input path="username" class="form-control"
					placeholder="Introdueix l'usuari" />
				<form:errors path="username" cssClass="error" />
			</div>
			<div class="form-group">
				<form:label path="password">Contrasenya:</form:label>
				<form:password path="password" class="form-control"
					placeholder="Introdueix la contrasenya" />
				<form:errors path="password" cssClass="error" />
			</div>
			<input class="btn btn-success" type="submit" value="Accedir" />
			<a href="${pageContext.request.contextPath}/registre.html" title="Registrar nou compte">
      		  <button type="button" class="btn btn-primary" style="float:right;">Registrar-se</button></a>
		</form:form>
	</div>
</body>

</html>