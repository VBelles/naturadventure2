<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="header">

	<div class="login">
	<div class="btn-group" role="group" style="float:right;">
	  <!--  Elegim mostrar el botó de login, o bé el d'accés al perfil i logout depenent si estem dins del sistema -->
	  <c:choose>
	    <c:when test="${empty sessionScope.personaRegistrada}">
	      <a href="${pageContext.request.contextPath}/login.html">
	        <button type="button" class="btn bar btn-primary">Entrar</button></a>
	    </c:when>
	    <c:otherwise>
	    
	      <c:if test="${sessionScope.personaRegistrada.usuari.tipusUsuari == 'MONITOR'}">
	        <a href="${pageContext.request.contextPath}/monitor/index.html" title="Supervisions">
	        <button type="button" class="btn bar btn-default">
	        <span class="glyphicon glyphicon-home">
	        </span></button></a>
	      </c:if>
	      
	      <c:if test="${sessionScope.personaRegistrada.usuari.tipusUsuari == 'ADMINISTRADOR'}">
	        <a href="${pageContext.request.contextPath}/admin/reserves/pendents.html" title="Accedir a l'administració">
	        <button type="button" class="btn bar btn-default">
	        <span class="glyphicon glyphicon-home">
	        </span></button></a>
	      </c:if>
	      
	      <a href="${pageContext.request.contextPath}/reserves.html" title="Llista de les reserves">
	      <button type="button" class="btn bar btn-default">
	      <span class="glyphicon glyphicon-th-list">
	      </span></button></a>
	      
	      <a href="${pageContext.request.contextPath}/perfil.html" title="Editar el meu perfil">
	      <button type="button" class="btn bar btn-default">
	      <span class="glyphicon glyphicon-cog">
	      </span></button></a>
	    
	      <a href="${pageContext.request.contextPath}/logout.html" title="Tancar sessió">
	        <button type="button" class="btn bar btn-danger">
	        <span class="glyphicon glyphicon-off" style="font-size: 1em;">
	      </span></button></a>
	    
	    </c:otherwise>
	  </c:choose>
	  </div> <!-- /btn-group -->
	</div> <!-- /login -->
  
  <div class="logo">
	<a href="${pageContext.request.contextPath}/index.html">NaturAdventure</a>
  </div>
   
   
   <ul class="nav nav-tabs">
     <!-- Iterem cada categoria, i obtenim per a cada una la llista de tipus d'activitats que li corresponen
       http://stackoverflow.com/a/4142885 -->
     <c:forEach items="${categories}" var="categoria"  varStatus="count">
       <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">${categoria.nom} <span class="caret"></span></a>
         <ul class="dropdown-menu">
           <c:forEach items="${llistaTipusActivitats[count.index]}" var="tipusActivitat">
             <li><a href="${pageContext.request.contextPath}/tipus/${tipusActivitat.nom}.html">${tipusActivitat.nom}</a></li>
           </c:forEach>
         </ul>
       </li>
     </c:forEach>
   </ul>

</div> <!-- /header -->