<%@page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>

<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/jquery.js"></script>

<title>Editant perfil - Naturadventure</title>
<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/assets/css/style-login.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/assets/css/style-perfil.css" rel="stylesheet">

	<!-- Evitem que el usuari accedixca a esta pàgina si no ha iniciat sessió
	http://stackoverflow.com/a/5411567 -->
	<c:if test="${empty sessionScope.personaRegistrada}">
	  <meta http-equiv="refresh" content="0; url=${pageContext.request.contextPath}/index.html" />
	</c:if>
	
</head>

<body>

	<div class="container">
		<h2>Editant la informació personal</h2>
		<form:form  method="post" modelAttribute="personaRegistrada"
			action="${pageContext.request.contextPath}/perfil.html">

			<div class="form-group" style="display:none">
				<form:input path="infoPersonal.codi" class="form-control" readonly="true"/>
			</div>
			
			<div class="form-group">
				<form:label path="infoPersonal.nom">Nom:</form:label>
				<form:input path="infoPersonal.nom" class="form-control" readonly="true"/>
				<form:errors path="infoPersonal.nom" cssClass="error" />
			</div>

			<div class="form-group">
				<form:label path="infoPersonal.telefon">Teléfon:</form:label>
				<form:input path="infoPersonal.telefon" class="form-control" />
				<form:errors path="infoPersonal.telefon" cssClass="error" />
			</div>
			
			<div class="form-group">
				<form:label path="infoPersonal.email">Correu electrònic:</form:label>
				<form:input path="infoPersonal.email" class="form-control" />
				<form:errors path="infoPersonal.email" cssClass="error" />
			</div>
			
			<div class="form-group">
				<input class="btn btn-success" type="submit" value="Guardar" />
			</div>
			
		
		</form:form>
		
		<div class="error">${msg}</div>
		
	</div>
</body>
</html>