<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <jsp:include page="headtag.jsp" />
    <title>Manteniment de NaturAdventure</title>
  </head>

  <body>
    <div class="container">
      <jsp:include page="header.jsp"/>
      
      <div class="content">
        <h3><a href="inicialitzar_bd.html">Inicialitzar BD</a></h3>
        <h3><a href="crear_taules.html">Crea les taules</a></h3>
        <h3><a href="borrar_taules.html">Borra les taules</a></h3>
      </div> <!-- /content -->


    </div> <!-- /container -->
    <jsp:include page="bottomtag.jsp" />
  </body>
</html>