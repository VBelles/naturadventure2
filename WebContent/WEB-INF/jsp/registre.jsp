<%@page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>

<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/jquery.js"></script>

<title>Registre - Naturadventure</title>
<link href="${pageContext.request.contextPath}/assets/css/bootstrap.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/assets/css/style-login.css" rel="stylesheet">
	
	<!-- Evitem que el usuari accedixca a esta pàgina si ja ha iniciat sessió
	http://stackoverflow.com/a/5411567 -->
	<c:if test="${not empty sessionScope.personaRegistrada}">
	  <meta http-equiv="refresh" content="0; url=${pageContext.request.contextPath}/index.html" />
	</c:if>

</head>

<body>


	<div class="container">
		<h2>Registrar-se</h2>
		<form:form  method="post" modelAttribute="registerForm"
			action="${pageContext.request.contextPath}/registre.html">

			<div class="form-group">
				<form:label path="infoPersonal.nom">Nom:</form:label>
				<form:input path="infoPersonal.nom" class="form-control"
					placeholder="Introdueix el seu nom" />
				<form:errors path="infoPersonal.nom" cssClass="error" />
			</div>

			<div class="form-group">
				<form:label path="infoPersonal.telefon">Teléfon:</form:label>
				<form:input path="infoPersonal.telefon" class="form-control"
					placeholder="Introdueix el seu teléfon" />
				<form:errors path="infoPersonal.telefon" cssClass="error" />
			</div>
			
			<div class="form-group">
				<form:label path="infoPersonal.email">Correu electrònic:</form:label>
				<form:input path="infoPersonal.email" class="form-control"
					placeholder="Introdueix el seu email" />
				<form:errors path="infoPersonal.email" cssClass="error" />
			</div>

		
			<div class="form-group">
				<form:label path="usuari.username">Usuari:</form:label>
				<form:input path="usuari.username" class="form-control"
					placeholder="Introdueix un usuari" />
				<form:errors path="usuari.username" cssClass="error" />
			</div>
			<div class="form-group">
				<form:label path="usuari.password">Contrasenya:</form:label>
				<form:password id="contrasenya" path="usuari.password" class="form-control"
					placeholder="Introdueix una contrasenya" />
				<form:errors path="usuari.password" cssClass="error" />
			</div>

			<div class="form-group">
				<label> Confirmar contrasenya:</label>
				<input type="password"  id="contrasenyaRep" class="form-control"
					placeholder="Introdueix la contrasenya anterior" />
				<div id="contrasenyaNoConfirmada"></div>
				
			</div>
			
			<div class="form-group">
				<input class="btn btn-success" type="submit" value="Registrar-se" />
			</div>
				
			<div class="error">${msg}</div>


			
		</form:form>




	</div>
	
	<script type="text/javascript">
	$(document).ready(function(){
	    $("form").submit(function(e){
	    	if($("#contrasenyaRep").val() == $("#contrasenya").val())
	    		return true;
	    	$("#contrasenyaNoConfirmada").html("La contrasenya no coincedeix amb l'anterior");
	    	return false;
	    });
	});
</script>
</body>
</html>