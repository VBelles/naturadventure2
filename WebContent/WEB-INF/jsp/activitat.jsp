<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <jsp:include page="headtag.jsp" />
    <link href="${pageContext.request.contextPath}/assets/css/style-activitat.css" rel="stylesheet">
    <title>${activitat.nom} - NaturAdventure</title>
  </head>

  <body>
    <div class="container">
      <jsp:include page="header.jsp"/>
      
      <div class="content">
      
        <div class="col-md-9">
          <div class="panel panel-info">
            <div class="panel-heading">
              <h4>${activitat.nom}</h4>
            </div>
            <div class="panel-content">
              <img class="activitat" src="${pageContext.request.contextPath}/img/default.jpg"
                alt="${activitat.nom}" style="float:left;">
              <p>${activitat.descripcio}</p>
        	</div>
          </div>
        </div>
        
        <div class="col-md-3">
          <div class="panel panel-info">
            <div class="panel-heading">
              <h4>Reserva</h4>
            </div>
            <div class="panel-content">
              <ul class="list-group">
              
              <li class="list-group-item">Participants:
		        <span style="float:right;">
                  <img class="participants" src="${pageContext.request.contextPath}/img/participants.png" title="Nombre de participants">
                    ${activitat.minParticipants}-${activitat.maxParticipants}
                </span>
		      </li>
		        
		        <li class="list-group-item">Hora d'inici:
		        <span style="float:right;">
		          <c:choose>
	                <c:when test="${activitat.horaInici == 'MATI'}">
	                  <img class="horainici" src="${pageContext.request.contextPath}/img/hora_1.png" title="Per el matí">
	                </c:when>
	                <c:when test="${activitat.horaInici == 'VESPRADA'}">
	                  <img class="horainici" src="${pageContext.request.contextPath}/img/hora_2.png" title="Per la vesprada">
	                </c:when>
	                <c:when test="${activitat.horaInici == 'NIT'}">
	                  <img class="horainici" src="${pageContext.request.contextPath}/img/hora_3.png" title="Per la nit">
	                </c:when>
	              </c:choose>
                </span>
		        </li>
		        
		        <li class="list-group-item">Dificultat:
		            <span style="float:right;">
	                  <c:choose>
		                <c:when test="${activitat.nivell == 'INICIACIO'}">
		                  <img class="nivell" src="${pageContext.request.contextPath}/img/nivell_1.png" title="Apta per a tots">
		                </c:when>
		                <c:when test="${activitat.nivell == 'MITJA'}">
		                  <img class="nivell" src="${pageContext.request.contextPath}/img/nivell_2.png" title="Certa preparaciò necessària">
		                </c:when>
		                <c:when test="${activitat.nivell == 'EXPERT'}">
		                  <img class="nivell" src="${pageContext.request.contextPath}/img/nivell_3.png" title="Sols per a experts">
		                </c:when>
	                  </c:choose>
	                   </span>
		        </li>
		        
		        <li class="list-group-item">Preu:
		        <span style="float:right;">
                    desde ${activitat.preuPerPersona}<span class="glyphicon glyphicon-euro"></span>
                </span>
		        </li>
		        
		        <li class="list-group-item"><a href="${pageContext.request.contextPath}/reserva/${activitat.codi}.html">
		          <button type="button" class="btn btn-primary btn-reserva" style="">Reserva</button></a></li>
		      </ul>
        	</div>
          </div>
        </div>

      </div> <!-- /content -->


    </div> <!-- /container -->
    <jsp:include page="bottomtag.jsp" />
  </body>
</html>