﻿<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>

<head>


<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">



<title>Registrar monitor</title>


<!-- stils -->
<jsp:include page="../stilsHeader.jsp" />

<!-- Include the plugin's CSS and JS: -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/assets/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/assets/css/bootstrap-multiselect.css"
	type="text/css" />

<style type="text/css">
</style>

<script type="text/javascript">
 
	$(document).ready(function(){
	    $("form").submit(function(e){
	    	if($("#contrasenyaRep").val() == $("#contrasenya").val())
	    		return true;
	    	$("#contrasenyaNoConfirmada").html("La contrasenya no coincedeix amb l'anterior");
	    	return false;
	    });
	    
	});
 
</script>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/assets/css/bootstrap-duallistbox.css"
	type="text/css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/assets/js/jquery.bootstrap-duallistbox.js"></script>


</head>

<body>



	<div id="wrapper">

		<!-- menu -->
		<jsp:include page="../menu.jsp" />
		<div id="page-wrapper">

			<div class="container-fluid">
				<div class="page-header">
					<h1>Registrar un monitor</h1>
				</div>

				<form:form class="form-horizontal" role="form" method="post"
					modelAttribute="monitor" accept-charset="UTF-8">
					<div class="form-group">
						<form:label path="infoPersonal.nom" class="control-label col-sm-3">Nom:</form:label>
						<div class="col-sm-8">
							<form:input path="infoPersonal.nom" type="text"
								class="form-control" />
							<span class="help-block"><form:errors
									path="infoPersonal.nom" cssClas="error" /></span>
						</div>
					</div>

					<div class="form-group">
						<form:label path="infoPersonal.telefon"
							class="control-label col-sm-3">Telèfon:</form:label>
						<div class="col-sm-8">
							<form:input path="infoPersonal.telefon" type="text"
								class="form-control" />
							<span class="help-block"><form:errors
									path="infoPersonal.telefon" cssClas="error" /></span>
						</div>
					</div>

					<div class="form-group">
						<form:label path="infoPersonal.email"
							class="control-label col-sm-3">Correu electrònic:</form:label>
						<div class="col-sm-8">
							<form:input path="infoPersonal.email" type="text"
								class="form-control" />
							<span class="help-block"><form:errors
									path="infoPersonal.email" cssClas="error" /></span>
						</div>
					</div>


					<div class="form-group">
						<form:label path="tipusActivitats" class="control-label col-sm-3">Tipus d'activitats:</form:label>
						<div class="col-sm-8">
							<form:select path="tipusActivitats" multiple="multiple" id="tipusActivitats" name="tipusActivitats">
							<c:forEach items="${monitor.tipusActivitats}" var="tipusActivitat"  varStatus="count">
							<option value="${tipusActivitat}" selected="selected">${tipusActivitat}</option>
							</c:forEach>
							<c:forEach items="${restaTipusActivitats}" var="tipusActivitat"  varStatus="count">
							<option value="${tipusActivitat}">${tipusActivitat}</option>
							</c:forEach>
							</form:select>
						</div>
					</div>

					
					<script>
            			var demo1 = $('select[name="tipusActivitats"]').bootstrapDualListbox();
            			var demo2 = $("#tipusActivitats").bootstrapDualListbox();
          			</script>
  


					<div class="form-group">
						<form:label path="usuari.username" class="control-label col-sm-3">Nom d'usuari:</form:label>
						<div class="col-sm-8">
							<form:input path="usuari.username" type="text"
								class="form-control" />
							<span class="help-block"><form:errors
									path="usuari.username" cssClas="error" /></span>
						</div>
					</div>

					<div class="form-group">
						<form:label path="usuari.password" class="control-label col-sm-3">Contrasenya provisional:</form:label>
						<div class="col-sm-8">
							<form:input id="contrasenya" path="usuari.password"
								type="password" class="form-control" />
							<span class="help-block">Aquesta contrasenya la canviarà
								el monitor al iniciar sesió</span> <span class="help-block"><form:errors
									path="usuari.password" cssClas="error" /></span>
						</div>
					</div>





					

					<div class="form-group">
						<label class="control-label col-sm-3">Confirmar
							contrasenya:</label>
						<div class="col-sm-8">
							<input id="contrasenyaRep" type="password" class="form-control" />
							<span id="contrasenyaNoConfirmada" class="help-block"></span>
						</div>
					</div>

					<div class="form-group">
						<div align="center">
							<button type="submit" class="btn btn-default">Registrar
								monitor</button>
						</div>

					</div>
				</form:form>


			</div>
			<!-- /#container-fluid -->

		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->




</body>

</html>
