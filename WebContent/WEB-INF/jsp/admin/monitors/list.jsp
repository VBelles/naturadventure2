<%@page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Llista monitors</title>

<!-- stils -->
<jsp:include page="../stilsHeader.jsp" />


</head>

<body>

	<div id="wrapper">

		<!-- menu -->
		<jsp:include page="../menu.jsp"/>

		<div id="page-wrapper">

			<div class="container-fluid">

				<h1>Llista de monitors</h1>
				<table class="table table-striped">
					<tr>
						<th>Codi</th>
						<th>Nom</th>
						<th>Telèfon</th>
						<th>Correu electrònic</th>
						<th>Usuari</th>
					</tr>
					<c:forEach items="${monitors}" var="monitor">
						<tr>
							<td>${monitor.infoPersonal.codi}</td>
							<td>${monitor.infoPersonal.nom}</td>
							<td>${monitor.infoPersonal.email}</td>
							<td>${monitor.infoPersonal.telefon}</td>
							<td>${monitor.usuari.username}</td>
							<td><a href="update/${monitor.usuari.username}.html" class="btn btn-default" role="button">Editar</a>
							<td><a href="delete/${monitor.usuari.username}.html" class="btn btn-danger" role="button">Esborrar</a>
						</tr>
					</c:forEach>

				</table>
				<a href="add.html" class="btn btn-success" role="button">Afegir monitor</a>

			</div>
			<!-- /.container-fluid -->

		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="${pageContext.request.contextPath}/assets/js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>

</body>

</html>
