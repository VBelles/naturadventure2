<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>

<head>


<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">



<title>Editar monitor</title>


<!-- stils -->
<jsp:include page="../stilsHeader.jsp" />

<!-- Include the plugin's CSS and JS: -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/assets/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/assets/css/bootstrap-multiselect.css"
	type="text/css" />

<style type="text/css">
</style>


<link rel="stylesheet"
	href="${pageContext.request.contextPath}/assets/css/bootstrap-duallistbox.css"
	type="text/css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/assets/js/jquery.bootstrap-duallistbox.js"></script>

</head>

<body>

	<div id="wrapper">

		<!-- menu -->
		<jsp:include page="../menu.jsp" />
		<div id="page-wrapper">

			<div class="container-fluid">
				<div class="page-header">
					<h1>Editar dades monitor: ${monitor.usuari.username} </h1>
				</div>

				<form:form class="form-horizontal" role="form" method="post"
					modelAttribute="monitor" accept-charset="UTF-8">
					
					<div class="form-group">
						<form:label path="infoPersonal.codi" class="control-label col-sm-3">Codi:</form:label>
						<div class="col-sm-8">
							<form:input path="infoPersonal.codi" type="text"
								class="form-control" readonly="true"/>
							<span class="help-block"><form:errors
									path="infoPersonal.nom" cssClas="error" /></span>
						</div>
					</div>

					<div class="form-group">
						<form:label path="infoPersonal.nom" class="control-label col-sm-3">Nom:</form:label>
						<div class="col-sm-8">
							<form:input path="infoPersonal.nom" type="text"
								class="form-control" />
							<span class="help-block"><form:errors
									path="infoPersonal.nom" cssClas="error" /></span>
						</div>
					</div>

					<div class="form-group">
						<form:label path="infoPersonal.telefon"
							class="control-label col-sm-3">Telèfon:</form:label>
						<div class="col-sm-8">
							<form:input path="infoPersonal.telefon" type="text"
								class="form-control" />
							<span class="help-block"><form:errors
									path="infoPersonal.telefon" cssClas="error" /></span>
						</div>
					</div>

					<div class="form-group">
						<form:label path="infoPersonal.email"
							class="control-label col-sm-3">Correu electrònic:</form:label>
						<div class="col-sm-8">
							<form:input path="infoPersonal.email" type="text"
								class="form-control" />
							<span class="help-block"><form:errors
									path="infoPersonal.email" cssClas="error" /></span>
						</div>
					</div>

					<div class="form-group">
						<form:label path="tipusActivitats" class="control-label col-sm-3">Tipus d'activitats:</form:label>
						<div class="col-sm-8">
							<form:select path="tipusActivitats" multiple="multiple" id="tipusActivitats" name="tipusActivitats">
							<c:forEach items="${monitor.tipusActivitats}" var="tipusActivitat"  varStatus="count">
							<option value="${tipusActivitat}" selected="selected">${tipusActivitat}</option>
							</c:forEach>
							<c:forEach items="${restaTipusActivitats}" var="tipusActivitat"  varStatus="count">
							<option value="${tipusActivitat}">${tipusActivitat}</option>
							</c:forEach>
							</form:select>
						</div>
					</div>

					
					<script>
            			var demo1 = $('select[name="tipusActivitats"]').bootstrapDualListbox();
          			</script>
  

					<div class="form-group">
						<form:label path="usuari.username" class="control-label col-sm-3">Nom d'usuari:</form:label>
						<div class="col-sm-8">
							<form:input path="usuari.username" type="text"
								class="form-control" readonly="true"/>
							<span class="help-block"><form:errors
									path="usuari.username" cssClas="error" />L'usuari no es pot canviar</span>
						</div>
					</div>
					
					<div class="form-group">
						<div align="center">
							<button type="submit" class="btn btn-default">Desar canvis</button>
						</div>

					</div>
				</form:form>


			</div>
			<!-- /#container-fluid -->

		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->




</body>

</html>
