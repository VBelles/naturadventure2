<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="naturadventure.model.PersonaRegistrada" %>


<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<a class="navbar-brand" href="${pageContext.request.contextPath}/index.html">Tornar a NaturAdventure</a>
	</div>
	<!-- Top Menu Items -->
	<ul class="nav navbar-right top-nav">

		<li class="dropdown"><a href="#" class="dropdown-toggle"
			data-toggle="dropdown"><i class="fa fa-user"></i> <%=session.getAttribute("nomUsuari") %> <b
				class="caret"></b></a>
			<ul class="dropdown-menu">
				<li><a href="${pageContext.request.contextPath}/admin/canviarContrasenya.html"><i class="glyphicon glyphicon-lock"></i> Canviar contrasenya</a></li>
				<li><a href="${pageContext.request.contextPath}/logout.html"><i class="glyphicon glyphicon-off"></i> Log Out</a></li>
				</ul>
			</li>
	</ul>
	
	<!-- MENU LATERAL -->
	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<ul class="nav navbar-nav side-nav">
			<li><a
				href="${pageContext.request.contextPath}/admin/monitors/list.html"><i
					class="fa fa-circle"></i> Monitors</a></li>
			
			<li><a href="javascript:;" data-toggle="collapse" onmouseover="hideshow(this)"
				data-target="#activitats"><i class="fa fa-fw fa-arrows-v"></i>
					Activitats <i class="fa fa-fw fa-caret-down"></i></a>
				<ul id="activitats" class="collapse">
				
					<c:forEach items="${categories}" var="categoria"  varStatus="count">
	                <li><a href="javascript:;" data-toggle="collapse"
					data-target="#${categoria}"><i class="fa fa-fw fa-arrows-v"></i>
					${categoria} <i class="fa fa-fw fa-caret-down"></i></a>
					<ul id="${categoria}" class="collapse">
						<li><a class="activitat" 
						href="${pageContext.request.contextPath}/admin/activitats/${categoria}.html" role="button">Mostrar totes </a>
						</li>
	                    <c:forEach items="${llistaTipusActivitats[count.index]}" var="tipusActivitat">
	                    
	                      <li>
	                      <a class="activitat" href="${pageContext.request.contextPath}/admin/activitats/tipus/${tipusActivitat.nom}.html" role="button">${tipusActivitat.nom}</a>
						
						</li>
	                    </c:forEach>
	                  </ul>
	                </li>
	              </c:forEach>
	             
				</ul></li>
				
				
			<li><a href="javascript:;" data-toggle="collapse"
				data-target="#reserves"><i class="fa fa-fw fa-arrows-v"></i>
					Reserves <i class="fa fa-fw fa-caret-down"></i></a>
				<ul id="reserves" class="collapse">
				<li><a
				href="${pageContext.request.contextPath}/admin/reserves/pendents.html"><i
					class="fa fa-circle"></i> Pendents</a></li>
					<li><a
				href="${pageContext.request.contextPath}/admin/reserves/acceptades.html"><i
					class="fa fa-circle"></i> Acceptades</a></li>
					<li><a
				href="${pageContext.request.contextPath}/admin/reserves/rebutjades.html"><i
					class="fa fa-circle"></i> Rebutjades</a></li>
				</ul>
			</li>
			
			<li><a
				href="${pageContext.request.contextPath}/admin/clients/registrats.html"><i
					class="fa fa-circle"></i> Clients</a></li>
			<li>
			
		</ul>
	</div>
	<!-- /.navbar-collapse -->
</nav>