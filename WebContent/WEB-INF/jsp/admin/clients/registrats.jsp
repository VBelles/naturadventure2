<%@page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Llista clients registrats</title>

<!-- stils -->
<jsp:include page="../stilsHeader.jsp" />


</head>

<body>

	<div id="wrapper">

		<!-- menu -->
		<jsp:include page="../menu.jsp"/>

		<div id="page-wrapper">

			<h1>Llista de clients registrats</h1>
			<table class="table table-striped">
				<tr>
					<th>Codi</th>
					<th>Nom</th>
					<th>Telèfon</th>
					<th>Correu electrònic</th>
					<th>Usuari</th>
				</tr>
				
				<c:forEach items="${personesRegistrades}" var="personaRegistrada">
					<tr>
						<td>${personaRegistrada.infoPersonal.codi}</td>
						<td>${personaRegistrada.infoPersonal.nom}</td>
						<td>${personaRegistrada.infoPersonal.telefon}</td>
						<td>${personaRegistrada.infoPersonal.email}</td>
						<td>${personaRegistrada.usuari.username}</td>
						<td><a href="registrats/delete/${personaRegistrada.usuari.username}.html" role="button" class="btn btn-danger">Eliminar</a>
					</tr>
				</c:forEach>

			</table>

			<!-- /.container-fluid -->

		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="${pageContext.request.contextPath}/assets/js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>

</body>

</html>
