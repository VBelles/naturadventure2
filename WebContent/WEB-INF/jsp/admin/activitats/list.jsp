<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Llista activitats</title>

<!-- stils -->
<jsp:include page="../stilsHeader.jsp" />

</head>

<body>

	<div id="wrapper">

		<!-- menu -->
		<jsp:include page="../menu.jsp"/>
		
		<div id="page-wrapper">

			<h1>Llista d'activitats</h1>
			<table class="table table-striped">
				<tr>
					<th>Codi</th>
					<th>Nom</th>
					<th>Tipus activitat</th>
					<th>Nivell</th>
					<th>Durada hores</th>
					<th>Hora inici</th>
					<th>Preu / persona</th>
					<th>Minims participants</th>
					<th>Maxims participants</th>
					<th>Descripció</th>

				</tr>
				<c:forEach items="${activitats}" var="activitat">
					<tr>
						<td>${activitat.codi}</td>
						<td>${activitat.nom}</td>
						<td>${activitat.tipusActivitat}</td>
						<td>${activitat.nivell.nivell}</td>
						<td>${activitat.duradaHores}</td>
						<td>${activitat.horaInici.hora}</td>
						<td>${activitat.preuPerPersona}</td>
						<td>${activitat.minParticipants}</td>
						<td>${activitat.maxParticipants}</td>
						<td>${activitat.descripcio}</td>
						<td><a href="${pageContext.request.contextPath}/admin/activitats/update/${activitat.codi}.html" class="btn btn-default" role="button">Editar</a>
						<td><a href="${pageContext.request.contextPath}/admin/activitats/delete/${activitat.codi}.html" class="btn btn-danger" role="button">Eliminar</a>
					</tr>
				</c:forEach>

			</table>
				
				<a href="${pageContext.request.contextPath}/admin/activitats/add.html" class="btn btn-success" role="button">Afegir activitat</a>
			<!-- /.container-fluid -->

		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="${pageContext.request.contextPath}/assets/js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>

</body>

</html>
