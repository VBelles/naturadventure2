﻿<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>

<head>


<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">


<title>Afegir activitat</title>


<!-- stils -->
<jsp:include page="../stilsHeader.jsp" />
<style type="text/css">

</style>

<script type="text/javascript">

 
	$(document).ready(function(){
	    $("form").submit(function(e){
	    	if($("#duradaHores").val() == "")
	    		$("#duradaHores").val(0);
	    	if($("#preuPerPersona").val() == "")
	    		$("#preuPerPersona").val(0);
	    	if($("#minParticipants").val() == "")
	    		$("#minParticipants").val(0);
	    	if($("#maxParticipants").val() == "")
	    		$("#maxParticipants").val(0);
	    	return true;
	    });
	    
	});

 
</script>


</head>

<body>


	<div id="wrapper">

		<!-- menu -->
		<jsp:include page="../menu.jsp" />
		<div id="page-wrapper">

			<div class="container-fluid">
				<div class="page-header">
					<h1>Afegir una activitat</h1>
				</div>

				<form:form class="form-horizontal" role="form" method="post"
					modelAttribute="activitat" accept-charset="UTF-8">
					<div class="form-group">
						<form:label path="nom" class="control-label col-sm-3">Nom:</form:label>
						<div class="col-sm-8">
							<form:input path="nom" type="text" class="form-control" />
							<span class="help-block"><form:errors path="nom"
									cssClas="error" /></span>
						</div>
					</div>

					<div class="form-group">
						<form:label path="tipusActivitat" class="control-label col-sm-3">Tipus d'activitat:</form:label>
						<div class="col-sm-8" >
							<form:select  path="tipusActivitat" class="form-control" title='Selecciona el tipus'>
								<form:option value="">--Seleccionar--</form:option>
                				<form:options items="${llistaTipusActivitat}"/>
            				</form:select>
							<span class="help-block"><form:errors path="tipusActivitat"
									cssClas="error" /></span>
						</div>
					</div>

					<div class="form-group">
						<form:label path="descripcio" class="control-label col-sm-3">Descripció:</form:label>
						<div class="col-sm-8">
							<form:textarea path="descripcio" type="text" class="form-control" />
							<span class="help-block"><form:errors path="descripcio"
									cssClas="error" /></span>
						</div>
					</div>

					<div class="form-group">
						<form:label path="horaInici" class="control-label col-sm-3">Hora d'inici:</form:label>
						<div class="col-sm-8">
							
							<form:select  path="horaInici" class="form-control" title="Selecciona l'hora d'inici">
								<form:option value="">--Seleccionar--</form:option>
                				<form:options items="${hores}"/>
            				</form:select>
							<span class="help-block"><form:errors path="horaInici"
									cssClas="error" /></span>
						</div>
					</div>

					<div class="form-group">
						<form:label path="duradaHores" class="control-label col-sm-3">Durada en hores:</form:label>
						<div class="col-sm-8">
							<form:input path="duradaHores" type="text" class="form-control" />
							<span class="help-block"><form:errors path="duradaHores"
									cssClas="error" /></span>
						</div>
					</div>

					<div class="form-group">
						<form:label path="nivell" class="control-label col-sm-3">Nivell:</form:label>
						<div class="col-sm-8">
							<form:select  path="nivell" class="form-control" title="Selecciona el nivell">
								<form:option value="">--Seleccionar--</form:option>
                				<form:options items="${nivells}"/>
            				</form:select>
							<span class="help-block"><form:errors path="nivell"
									cssClas="error" /></span>
						</div>
					</div>

					<div class="form-group">
						<form:label path="preuPerPersona" class="control-label col-sm-3">Preu per persona:</form:label>
						<div class="col-sm-8">
							<form:input path="preuPerPersona" type="text"
								class="form-control" />
							<span class="help-block"><form:errors
									path="preuPerPersona" cssClas="error" /></span>
						</div>
					</div>

					<div class="form-group">
						<form:label path="minParticipants" class="control-label col-sm-3">Nº min participants:</form:label>
						<div class="col-sm-8">
							<form:input path="minParticipants" type="text"
								class="form-control" />
							<span class="help-block"><form:errors
									path="minParticipants" cssClas="error" /></span>
						</div>
					</div>

					<div class="form-group">
						<form:label path="maxParticipants" class="control-label col-sm-3">Nº max participants:</form:label>
						<div class="col-sm-8">
							<form:input path="maxParticipants" type="text"
								class="form-control" />
							<span class="help-block"><form:errors
									path="maxParticipants" cssClas="error" /></span>
						</div>
					</div>


					<div class="form-group">
						<div align="center">
							<button type="submit" class="btn btn-default">Afegir
								activitat</button>
								</div>
						
					</div>
				</form:form>
				

			</div>
			<!-- /#container-fluid -->

		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->


	<!-- jQuery -->
	<script src="${pageContext.request.contextPath}/assets/js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>

</body>

</html>
