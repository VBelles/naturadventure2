<%@page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Reserves pendents</title>

<!-- stils -->
<jsp:include page="../stilsHeader.jsp" />


</head>

<body>

	<div id="wrapper">

		<!-- menu -->
		<jsp:include page="../menu.jsp"/>

		<div id="page-wrapper">

			<h1>Llista de reserves pendents d'acceptació</h1>
			<table class="table table-striped">
				<tr>
					<th>Codi</th>
					<th>Tipus activitat</th>
					<th>Nom activitat</th>
					<th>Nº Participants</th>
					<th>Data activitat</th>
					<th>Hora d'inici</th>
					<th>Data reserva</th>
					<th>Preu final</th>
					<th>Estat</th>
				</tr>
				
				<c:forEach items="${reservesActivitats}" var="reservaActivitat" varStatus="status">
					<tr>
						<td>${reservaActivitat.reserva.codi}</td>
						<td>${reservaActivitat.activitat.tipusActivitat}</td>
						<td>${reservaActivitat.activitat.nom}</td>
						<td>${reservaActivitat.reserva.numParticipants}</td>
						<td>${reservaActivitat.reserva.dataActivitat}</td>
						<td>${reservaActivitat.reserva.horaInici.hora}</td>
						<td>${reservaActivitat.reserva.dataReserva}</td>
						<td>${reservaActivitat.reserva.preuFinal}</td>
						<td>${reservaActivitat.reserva.estat}</td>
						<td><a
							href="${pageContext.request.contextPath}/admin/reserves/assignar/${reservaActivitat.reserva.codi}.html" role="button" class="btn btn-default">Assignar monitor</a>
						<td><a
							href="${pageContext.request.contextPath}/admin/reserves/rebutjar/${reservaActivitat.reserva.codi}.html" role="button" class="btn btn-danger">Rebutjar</a>
					</tr>
				</c:forEach>

			</table>
			

			<!-- /.container-fluid -->

		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="${pageContext.request.contextPath}/assets/js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>

</body>

</html>
