<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>

<head>


<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">


<title>Index Administració</title>


<!-- stils -->
<jsp:include page="stilsHeader.jsp" />
<style type="text/css">

</style>

</head>

<body>



	<div id="wrapper">

		<!-- menu -->
		<jsp:include page="menu.jsp" />
		<div id="page-wrapper">

			<div class="container-fluid">
				<div class="page-header">
					<h1>Página d'inici</h1>
				</div>

			</div>
			<!-- /#container-fluid -->

		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->


	<!-- jQuery -->
	<script src="${pageContext.request.contextPath}/assets/js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>

</body>

</html>
