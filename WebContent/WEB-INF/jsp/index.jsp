<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <jsp:include page="headtag.jsp" />
    <title>NaturAdventure</title>
  </head>

  <body>
    <div class="container">
      <jsp:include page="header.jsp"/>
      
      <div class="content">

        <h3>Activitats més demanades</h3>
        <div class="row">
          <c:forEach items="${demanades}" var="activitat">
            <div class="col-xs-6 col-md-4">
              <div class="thumbnail">
               <a href="${pageContext.request.contextPath}/activitat/${activitat.codi}.html">
                 <img class="activitat" src="${pageContext.request.contextPath}/img/default.jpg" alt="${activitat.nom}"></a>
                 <div class="caption">
                   <a href="${pageContext.request.contextPath}/activitat/${activitat.codi}.html"><h4>${activitat.nom}</h4></a>
                   <p>${activitat.descripcio}</p>
                   
                   <span style="float:right; padding-bottom:10px; padding-left:10px">
                      desde ${activitat.preuPerPersona}<span class="glyphicon glyphicon-euro"></span>
                    </span>
                    
                    <span style="float:right; padding-bottom:10px; padding-left: 10px">
	                  <c:choose>
		                <c:when test="${activitat.nivell == 'INICIACIO'}">
		                  <img class="nivell" src="${pageContext.request.contextPath}/img/nivell_1.png" title="Apta per a tots">
		                </c:when>
		                <c:when test="${activitat.nivell == 'MITJA'}">
		                  <img class="nivell" src="${pageContext.request.contextPath}/img/nivell_2.png" title="Certa preparaciò necessària">
		                </c:when>
		                <c:when test="${activitat.nivell == 'EXPERT'}">
		                  <img class="nivell" src="${pageContext.request.contextPath}/img/nivell_3.png" title="Sols per a experts">
		                </c:when>
	                  </c:choose>
                    </span>
                    
                    <span style="float:right; padding-bottom:10px;">
                      <img class="participants" src="${pageContext.request.contextPath}/img/participants.png" title="Nombre de participants">
                      ${activitat.minParticipants}-${activitat.maxParticipants}
                    </span>
                    
            	 </div>
              </div>
            </div>
          </c:forEach>
        </div>

        <h3>Activitats més recents</h3>
        <div class="row">
          <c:forEach items="${recents}" var="activitat">
            <div class="col-xs-6 col-md-4">
              <div class="thumbnail">
               <a href="${pageContext.request.contextPath}/activitat/${activitat.codi}.html">
                 <img class="activitat" src="${pageContext.request.contextPath}/img/default.jpg" alt="${activitat.nom}"></a>
                 <div class="caption">
                   <a href="${pageContext.request.contextPath}/activitat/${activitat.codi}.html"><h4>${activitat.nom}</h4></a>
                   <p>${activitat.descripcio}</p>
                   
                   <span style="float:right; padding-bottom:10px; padding-left:10px">
                      desde ${activitat.preuPerPersona}<span class="glyphicon glyphicon-euro"></span>
                    </span>
                    
                    <span style="float:right; padding-bottom:10px; padding-left: 10px">
	                  <c:choose>
		                <c:when test="${activitat.nivell == 'INICIACIO'}">
		                  <img class="nivell" src="${pageContext.request.contextPath}/img/nivell_1.png" title="Apta per a tots">
		                </c:when>
		                <c:when test="${activitat.nivell == 'MITJA'}">
		                  <img class="nivell" src="${pageContext.request.contextPath}/img/nivell_2.png" title="Certa preparaciò necessària">
		                </c:when>
		                <c:when test="${activitat.nivell == 'EXPERT'}">
		                  <img class="nivell" src="${pageContext.request.contextPath}/img/nivell_3.png" title="Sols per a experts">
		                </c:when>
	                  </c:choose>
                    </span>
                    
                    <span style="float:right; padding-bottom:10px;">
                      <img class="participants" src="${pageContext.request.contextPath}/img/participants.png" title="Nombre de participants">
                      ${activitat.minParticipants}-${activitat.maxParticipants}
                    </span>
                    
            	 </div>
              </div>
            </div>
          </c:forEach>
        </div>

      </div> <!-- /content -->

    </div> <!-- /container -->
    <jsp:include page="bottomtag.jsp" />
  </body>
</html>