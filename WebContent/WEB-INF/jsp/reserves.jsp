<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <jsp:include page="headtag.jsp" />
    <title>NaturAdventure</title>
  </head>

  <body>
    <div class="container">
      <jsp:include page="header.jsp"/>
      
      <div class="content">

        <h3>Reserves</h3>
        <div class="row">
          <table class="table table-striped">
				<tr>
					<th>Tipus activitat</th>
					<th>Nom activitat</th>
					<th>Nº Participants</th>
					<th>Data activitat</th>
					<th>Hora d'inici</th>
					<th>Data reserva</th>
					<th>Preu final</th>
					<th>Estat</th>
				</tr>
				
				<c:forEach items="${reserves}" var="reservaActivitat" varStatus="status">
					<tr>
						<td>${reservaActivitat.activitat.tipusActivitat}</td>
						<td>${reservaActivitat.activitat.nom}</td>
						<td>${reservaActivitat.reserva.numParticipants}</td>
						<td>${reservaActivitat.reserva.dataActivitat}</td>
						<td>${reservaActivitat.reserva.horaInici.hora}</td>
						<td>${reservaActivitat.reserva.dataReserva}</td>
						<td>${reservaActivitat.reserva.preuFinal}</td>
						<td>${reservaActivitat.reserva.estat}</td>
						
					</tr>
				</c:forEach>

			</table>
        </div>

      </div> <!-- /content -->

    </div> <!-- /container -->
    <jsp:include page="bottomtag.jsp" />
  </body>
</html>