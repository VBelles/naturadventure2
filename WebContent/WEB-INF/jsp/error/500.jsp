<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <jsp:include page="../headtag.jsp" />
    <title>Error 500</title>
  </head>

  <body>
    <div class="container">
      <jsp:include page="../header.jsp"/>
      
      <div class="content">

        <h3>Error 500</h3>
        <h4>Fallo intern del sistema</h4>
        

      </div> <!-- /content -->

    </div> <!-- /container -->
    <jsp:include page="../bottomtag.jsp" />
  </body>
</html>