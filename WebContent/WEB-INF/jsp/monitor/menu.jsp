<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="naturadventure.model.PersonaRegistrada" %>


<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">

		<a class="navbar-brand" href="${pageContext.request.contextPath}/monitor/index.html">NaturAdventure</a>
	</div>
	<!-- Top Menu Items -->
	<ul class="nav navbar-right top-nav">

		<li class="dropdown"><a href="#" class="dropdown-toggle"
			data-toggle="dropdown"><i class="fa fa-user"></i> <%=  session.getAttribute("nomUsuari") %> <b
				class="caret"></b></a>
				<ul class="dropdown-menu">
				<li><a href="${pageContext.request.contextPath}/monitor/perfil.html"></i>Modificar perfil</a></li>
				<li><a href="${pageContext.request.contextPath}/monitor/canviarContrasenya.html"></i>Canviar contrasenya</a></li>
				<li><a href="${pageContext.request.contextPath}/logout.html"><i class="fa fa-fw fa-power-off"></i> Log Out</a></li>
				</ul>
			
			</ul>
	
	</ul>
	
	<!-- MENU LATERAL -->
	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<ul class="nav navbar-nav side-nav">
		<li><a
				href="${pageContext.request.contextPath}/monitor/index.html"><i
					class="fa fa-circle"></i> Calendari</a></li>
					
		<li><a
				href="${pageContext.request.contextPath}/monitor/list.html"><i
					class="fa fa-circle"></i> Llista</a></li>
			
		</ul>
	</div>
	<!-- /.navbar-collapse -->
</nav>