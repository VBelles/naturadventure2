<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>

<head>


<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">


<title>Activitats per realitzar</title>


<!-- stils -->
<jsp:include page="stilsHeader.jsp" />
<style type="text/css">

table.calendar {
  margin: auto;
}
h1.calendar, h2.calendar, h3.calendar {
  text-align: center;
}
input[type=submit]{
  width: 120px;
  height: 80px;
  margin: 0px;
  padding: 0px;

  font-weight: bold;
  font-size: 220%;
  text-transform: uppercase;
}
button {
  width: 120px;
  height: 80px;
  margin: 0px;
  padding: 0px;

  font-weight: bold;
  font-size: 220%;
  text-transform: uppercase;
}
button.flecha {
  background-color: Transparent;
  background-repeat: no-repeat;
  border: none;
  cursor: pointer;
  overflow: hidden;
  outline: none;
}

}

</style>

</head>

<body>

	<div id="wrapper">

		<!-- menu -->
		<jsp:include page="menu.jsp" />
		<div id="page-wrapper">

			<div class="container-fluid">
				<div class="page-header">
					<h1>Calendari de les activitats per supervisar</h1>
				</div>
				
				<div id="calendari">
				</div>
				<div id="eventsDia"></div>
	
	

	<table class ='calendar'><tr>

	
	<div align="center"> 
	<h1 class="calendari"><a href="${pageContext.request.contextPath}/monitor/calendari/${dia}/${mes-1}/${any}.html"><</a> 
	${nomMes} 
	<a href="${pageContext.request.contextPath}/monitor/calendari/${dia}/${mes+1}/${any}.html">></a></h1>
	</div>
	<!-- Dies setmana -->
	<c:forEach var="dia" items="${diesSetmana}">
   		<th class='header'><h3>${dia}</h3></th>
	</c:forEach>
	</tr>
	<tr>
	
	
	<!-- Omplir huecos -->
	<c:forEach var="i" begin="1" end="${diaInicial-1}">
   		<th></th>
	</c:forEach>
	
	<!-- Omplir dies -->
	<c:forEach var="i" begin="1" end="${diesMes}" varStatus="counter">
	<c:choose>
          <c:when test="${(counter.count + diaInicial - 2) % 7 == 0}">
            </tr><tr>
            
            <th><a href="${pageContext.request.contextPath}/monitor/calendari/${i}/${mes}/${any}.html">
              <button name="dia"  id="${i}" value="${i}">${i}</button></a></th>
          </c:when>
          <c:otherwise>
              <th><a href="${pageContext.request.contextPath}/monitor/calendari/${i}/${mes}/${any}.html">
              <button name="dia"  id="${i}" value="${i}">${i}</button></a></th>
          </c:otherwise>
        </c:choose>
	</c:forEach>
	</tr>
	</table>
	
	<br>
	<br>
	<h1> Reserves del dia ${dia}/${mes}/${any}:</h1>
	<table class="table table-striped">
				<tr>
					<th>Codi</th>
					<th>Tipus activitat</th>
					<th>Nom activitat</th>
					<th>Nº Participants</th>
					<th>Data activitat</th>
					<th>Hora d'inici</th>
					<th>Data reserva</th>
					<th>Preu final</th>
				</tr>
				
				<c:forEach items="${reservesDia}" var="reservaActivitat" varStatus="status">
					<tr>
						<td>${reservaActivitat.reserva.codi}</td>
						<td>${reservaActivitat.activitat.tipusActivitat}</td>
						<td>${reservaActivitat.activitat.nom}</td>
						<td>${reservaActivitat.reserva.numParticipants}</td>
						<td>${reservaActivitat.reserva.dataActivitat}</td>
						<td>${reservaActivitat.reserva.horaInici.hora}</td>
						<td>${reservaActivitat.reserva.dataReserva}</td>
						<td>${reservaActivitat.reserva.preuFinal}</td>
					</tr>
				</c:forEach>

			</table>
			
			<script>
			<c:forEach items="${reservesMes}" var="diaReserva" varStatus="status">
			
			var element = ${diaReserva}
				document.getElementById(element).style.backgroundColor="#99FF66"
			</c:forEach>
			</script>
	
	


			</div>
			<!-- /#container-fluid -->

		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->


	<!-- jQuery -->
	<script src="${pageContext.request.contextPath}/assets/js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>

</body>

</html>
