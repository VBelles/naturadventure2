<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>

<head>


<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">


<title>Modificar perfil</title>


<!-- stils -->
<jsp:include page="stilsHeader.jsp" />
<style type="text/css">

.form-group{
max-width: 200px;

}

</style>

</head>

<body>

	<div id="wrapper">

		<!-- menu -->
		<jsp:include page="menu.jsp" />
		<div id="page-wrapper">

			<div class="container-fluid">
				<div class="page-header">
					<h1> Modificar perfil</h1>
				</div>
		
	<div class="perfil">
	<form:form  method="post" modelAttribute="personaRegistrada"
			action="${pageContext.request.contextPath}/monitor/perfil.html">
			
	
				<div class="form-group" style="display:none">
					<form:input path="infoPersonal.codi" class="form-control" readonly="true"/>
				</div>
				
				<div class="form-group">
					<form:label path="infoPersonal.nom">Nom:</form:label>
					<form:input path="infoPersonal.nom" class="form-control" readonly="true"/>
					<form:errors path="infoPersonal.nom" cssClass="error" />
				</div>

				<div class="form-group">
					<form:label path="infoPersonal.telefon">Teléfon:</form:label>
					<form:input path="infoPersonal.telefon" class="form-control" />
					<form:errors path="infoPersonal.telefon" cssClass="error" />
				</div>
				
				<div class="form-group">
					<form:label path="infoPersonal.email">Correu electrònic:</form:label>
					<form:input path="infoPersonal.email" class="form-control" />
					<form:errors path="infoPersonal.email" cssClass="error" />
				</div>
				<br>
				<div class="form-group">
					<input class="btn btn-primary" type="submit" value="Guardar" />
				</div>
		
		</form:form>
		
		<div class="error">${msg}</div>
			
		</div>
			</div>
			<!-- /#container-fluid -->

		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->


	<!-- jQuery -->
	<script src="${pageContext.request.contextPath}/assets/js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="${pageContext.request.contextPath}/assets/js/bootstrap.min.js"></script>

</body>

</html>
