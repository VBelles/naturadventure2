<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <jsp:include page="headtag.jsp" />
    <link href="${pageContext.request.contextPath}/assets/css/style-reserva.css" rel="stylesheet">
    <title>Reservant ${activitat.nom} - NaturAdventure</title>
  </head>

  <body>
    <div class="container">
      <jsp:include page="header.jsp"/>
      
      <div class="content">
	      <div class="form">
	      
			<h2>Reservant activitat:</h2>
			<h3>${activitat.nom}</h3>
			<form:form method="post" modelAttribute="clientReserva"
				action="${pageContext.request.contextPath}/reserva/${activitat.codi}.html">
				
				<div class="form-group" style="font-size: large;">
				  <p>Preu per persona: <span id="preuPerPersona">${activitat.preuPerPersona}</span><span class="glyphicon glyphicon-euro"></span></p>
				</div>
				
				<div class="form-group">
				  <form:label path="reserva.dataActivitat">Data de reserva:</form:label>
				  <form:input type="date" path="reserva.dataActivitat" class="form-control"/>
				  <form:errors path="reserva.dataActivitat" cssClass="error" />
				</div>
				
				<div class="form-group">
				  <form:label path="reserva.numParticipants">Participants:</form:label>
				  <form:select path="reserva.numParticipants" class="form-control" onchange="actualitzarPreu()" id="participants">
	                <form:option value="" disabled="true" selected="true" style="display:none;">Tria</form:option>
	                <c:forEach var="i" begin="${activitat.minParticipants}" end="${activitat.maxParticipants}">
	                  <form:option value="${i}">${i}</form:option>
	                </c:forEach>
	              </form:select>
	              <form:errors path="reserva.numParticipants" cssClass="error" />
	            </div>
	            
				<!-- Si la persona no s'ha loguejat, cal obtindre la seva informacio, de lo contrari la obtenim
				de la variable de sesió.  -->
				<c:if test="${empty sessionScope.personaRegistrada}">
				  
					<div class="form-group">
						<form:label path="infoPersonal.nom">Nom:</form:label>
						<form:input path="infoPersonal.nom" class="form-control"
							placeholder="Introdueix el nom complet" />
						<form:errors path="infoPersonal.nom" cssClass="error" />
					</div>
					<div class="form-group">
						<form:label path="infoPersonal.telefon">Teléfon:</form:label>
						<form:input path="infoPersonal.telefon" class="form-control"
							placeholder="Introdueix el seu teléfon" />
						<form:errors path="infoPersonal.telefon" cssClass="error" />
					</div>
					<div class="form-group">
						<form:label path="infoPersonal.email">Correu electrònic:</form:label>
						<form:input path="infoPersonal.email" class="form-control"
							placeholder="Introdueix el correu electrònic" />
						<form:errors path="infoPersonal.email" cssClass="error" />
					</div>
					
				</c:if>
				
				<div class="form-group" style="font-size: large;">
				  <p>Preu final: <span id="preuFinal"></span><span class="glyphicon glyphicon-euro"></span></p>
				</div>
	
				
				<input class="btn btn-primary" type="submit" value="Reservar" />
			</form:form>

		</div> <!-- /form -->

      </div> <!-- /content -->

    </div> <!-- /container -->
    <jsp:include page="bottomtag.jsp" />

    <script>
      // http://stackoverflow.com/a/20766330
      function getParticipants() {
        return $('#participants').val();
      }

      function getPreuPerPersona() {
        return parseFloat(document.getElementById('preuPerPersona').innerHTML);
      }

      function actualitzarPreu() {
        document.getElementById('preuFinal').textContent = getParticipants() * getPreuPerPersona();
      }
      
      //http://stackoverflow.com/a/6946347
      function getUrlReserva() {
    	  var urlActual = window.location.href;
    	  var urlReserva =  urlActual.substring(0, urlActual.length - 5) + "/" + getParticipants() + ".html";
    	  alert(urlReserva);
    	  return urlReserva;
      }
    </script>

  </body>
</html>