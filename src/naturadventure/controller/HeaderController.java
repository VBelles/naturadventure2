package naturadventure.controller;

import java.util.ArrayList;
import java.util.List;

import naturadventure.dao.CategoriaDao;
import naturadventure.dao.TipusActivitatDao;
import naturadventure.model.Categoria;
import naturadventure.model.TipusActivitat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class HeaderController {
	
	@Autowired
	CategoriaDao categoriaDao;
	@Autowired
	TipusActivitatDao tipusActivitatDao;
	
	
	/** Obtenim les categories per mostrarles en la barra de navegació superior. */
	@ModelAttribute("categories")
	public List<Categoria> getCategories() {
		return categoriaDao.getAll();
	}
	
	/** Obtenim la llista de tipus d'activitats per als dropdowns. */
	@ModelAttribute("llistaTipusActivitats")
	public ArrayList<List<TipusActivitat>> getCategories(Model model) {
		ArrayList<List<TipusActivitat>> llistaTipusActivitats = new ArrayList<List<TipusActivitat>>();
		for(Categoria c:categoriaDao.getAll())
			llistaTipusActivitats.add(tipusActivitatDao.getByCategoria(c.getNom()));
		return llistaTipusActivitats;
	}
	
}