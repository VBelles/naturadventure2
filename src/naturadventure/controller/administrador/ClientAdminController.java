package naturadventure.controller.administrador;

import naturadventure.dao.InfoPersonalDao;
import naturadventure.dao.UsuariDao;
import naturadventure.model.Usuari;
import naturadventure.model.ennumeration.TipusUsuari;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/admin/clients")
@Controller
public class ClientAdminController extends AdminController {

	@Autowired
	private UsuariDao usuariDao;
	@Autowired
	private InfoPersonalDao infoPersonalDao;


	
	// LLISTAR
	@RequestMapping(value = "/registrats")
	public String llistarClientsRegistrats(Model model) {
		model.addAttribute("personesRegistrades",
				usuariDao.getPersonaRegistrada(TipusUsuari.USUARI));
		return "admin/clients/registrats";
	}

	
	// ESBORRAR
	@RequestMapping(value = "/registrats/delete/{username}")
	public String processDelete(@PathVariable String username, Model model) {
		model.asMap().clear();
		model.addAttribute("msg", "Client amb usuari " + username +" eliminat");
		Usuari usuari = usuariDao.get(username);
		usuariDao.delete(username);
		infoPersonalDao.delete(usuari.getCodiInfo());
		return "redirect:../../registrats.html";
	}
	
	@RequestMapping(value = "/noRegistrats/delete/{codi}")
	public String processDelete(@PathVariable int codi, Model model) {
		model.asMap().clear();
		model.addAttribute("msg", "Client amb codi " + codi +" eliminat");
		infoPersonalDao.delete(codi);
		return "redirect:../../noRegistrats.html";
	}

}