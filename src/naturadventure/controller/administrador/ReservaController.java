package naturadventure.controller.administrador;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.http.HttpSession;

import naturadventure.dao.ReservaDao;
import naturadventure.dao.SupervisioDao;
import naturadventure.dao.UsuariDao;
import naturadventure.mail.GoogleMail;
import naturadventure.model.Activitat;
import naturadventure.model.InfoPersonal;
import naturadventure.model.Reserva;
import naturadventure.model.ennumeration.Estat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/admin/reserves")
@Controller
public class ReservaController extends AdminController {
	
	@Autowired
	private ReservaDao reservaDao;
	@Autowired
	private UsuariDao usuariDao;
	@Autowired
	private SupervisioDao supervisioDao;

	// LLISTAR
	@RequestMapping("/pendents")
	public String getReservesPendents(HttpSession session, Model model) {
		model.addAttribute("reservesActivitats", reservaDao.getReservaActivitat(Estat.PENDENT));
		return "admin/reserves/pendents";
	}
	
	@RequestMapping("/acceptades")
	public String getReservesAcceptades(HttpSession session, Model model) {
		model.addAttribute("reservesActivitats", reservaDao.getReservaActivitat(Estat.ACCEPTADA));
		return "admin/reserves/acceptades";
	}

	@RequestMapping("/rebutjades")
	public String getReservesRebutjades(HttpSession session, Model model) {
		model.addAttribute("reservesActivitats", reservaDao.getReservaActivitat(Estat.REBUTJADA));
		return "admin/reserves/rebutjades";
	}

	// ASSIGNAR
	@RequestMapping(value = "/assignar/{codiReserva}", method = RequestMethod.GET)
	public String listMonitors(Model model, @PathVariable int codiReserva) {

		String tipusActivitat = reservaDao.getTipusActivitat(codiReserva);

	
		model.addAttribute("monitors", 	usuariDao.getMonitors(tipusActivitat));
		model.addAttribute("codiReserva", codiReserva);
		return "admin/reserves/assignar";
	}

	//Asignar
	@RequestMapping(value = "/assignar/{codiReserva}/{usernameMonitor}", method = RequestMethod.GET)
	public String assignarMonitor(Model model, @PathVariable String codiReserva,
			@PathVariable String usernameMonitor) {	
		
		Reserva r = reservaDao.get(codiReserva);
		r.setUsernameMonitor(usernameMonitor);
		r.setEstat(Estat.ACCEPTADA);
		reservaDao.update(r);	
		model.asMap().clear();
		
		
		//Enviem un e-mail al client notificant-li el estat de la seva reserva
		int codi = Integer.parseInt(codiReserva);
		Activitat a = reservaDao.getActivitat(codi);
		InfoPersonal ip = reservaDao.getInfoPersonal(codi);
		Date data = r.getDataActivitat();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		String assumpte = "Confirmaci� de reserva de l'activitat " + a.getNom();
		String missatge = "<p>La reserva de l'activitat " + a.getNom() + " el " +
				dateFormat.format(data) + " per a " +r.getNumParticipants() + " persones ha sigut CONFIRMADA.</p>" +
				"<p>T'esperem. Gr�cies per confiar en NaturAdventure.</p>";
		
		try {
			GoogleMail.Send(ip.getEmail(), assumpte, missatge);
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		
		return "redirect:/admin/reserves/pendents.html";
	}
	
	// Rebutjar
	@RequestMapping(value = "/rebutjar/{codiReserva}", method = RequestMethod.GET)
	public String eliminarReserva(Model model, @PathVariable int codiReserva) {
		Reserva r = reservaDao.get(codiReserva);
		r.setEstat(Estat.REBUTJADA);
		reservaDao.update(r);
		model.asMap().clear();
		
		//Enviem un e-mail al client notificant-li el estat de la seva reserva
		Activitat a = reservaDao.getActivitat(codiReserva);
		InfoPersonal ip = reservaDao.getInfoPersonal(codiReserva);
		Date data = r.getDataActivitat();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		String assumpte = "Rebutjament de reserva de l'activitat " + a.getNom();
		String missatge = "<p>La reserva de l'activitat " + a.getNom() + " el " +
				dateFormat.format(data) + " per a " +r.getNumParticipants() + " persones ha sigut REBUTJADA.</p>" +
				"<p>No hi havien monitors disponibles per a la data que has triat. Intenta reservar de nou en uns dies. Gr�cies.</p>";
		
		try {
			GoogleMail.Send(ip.getEmail(), assumpte, missatge);
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		
		return "redirect:/admin/reserves/rebutjades.html";
	}

}