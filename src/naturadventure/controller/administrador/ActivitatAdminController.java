﻿package naturadventure.controller.administrador;

import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpSession;

import naturadventure.dao.ActivitatDao;
import naturadventure.model.Activitat;

import naturadventure.model.ennumeration.HoraInici;
import naturadventure.model.ennumeration.Nivell;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/admin/activitats")
@Controller
public class ActivitatAdminController extends AdminController{
	
	@Autowired
	private ActivitatDao activitatDao;

	// LLISTAR
	@RequestMapping(value = "/{categoria}", method = RequestMethod.GET)
	public String llistarActivitatsCategoria(Model model, @PathVariable String categoria,
			HttpSession session) {

		try {
			categoria = new String(categoria.getBytes("ISO-8859-1"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		model.addAttribute("activitats", activitatDao.getByCategoria(categoria));
		
		return "admin/activitats/list";
	}
	
	@RequestMapping(value = "/tipus/{tipusActivitat}", method = RequestMethod.GET)
	public String llistarActivitatsTipus(Model model, @PathVariable String tipusActivitat,
			HttpSession session) {
		
		//Necessari per no tindre errors amb els accents
		try {
			tipusActivitat = new String(tipusActivitat.getBytes("ISO-8859-1"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		model.addAttribute("activitats", activitatDao.getByTipusActivitat(tipusActivitat));
		
		return "admin/activitats/list";
	}

	// MODIFICAR
	@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
	public String editActivitat(Model model, @PathVariable String id,
			HttpSession session) {
		
		model.addAttribute("llistaTipusActivitat", tipusActivitatDao.getAllNoms());
		model.addAttribute("hores", HoraInici.values());
		model.addAttribute("nivells", Nivell.values());
		model.addAttribute("activitat", activitatDao.get(id));

		return "admin/activitats/update";
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	public String processUpdateSubmit(@PathVariable int id,
			@ModelAttribute("activitat") Activitat activitat,
			BindingResult bindingResult, Model model) {
		
		ActivitatValidator activitatValidator = new ActivitatValidator();
		activitatValidator.validate(activitat, bindingResult);
		if (bindingResult.hasErrors()) {
			//Si no s'afegix una altra vegada desapareix...
			model.addAttribute("llistaTipusActivitat", tipusActivitatDao.getAllNoms());
			model.addAttribute("hores", HoraInici.values());
			model.addAttribute("nivells", Nivell.values());
			return "admin/activitats/add";
		}

		activitatDao.update(activitat);
		model.asMap().clear();
		return "redirect:/admin/activitats/tipus/" + activitat.getTipusActivitat() + ".html";
	}

	// AFEGIR
	@RequestMapping(value = "/add")
	public String addAcvititat(Model model) {

		model.addAttribute("llistaTipusActivitat", tipusActivitatDao.getAllNoms());
		model.addAttribute("hores", HoraInici.values());
		model.addAttribute("nivells", Nivell.values());
		model.addAttribute("activitat", new Activitat());
		return "admin/activitats/add";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String processAddSubmit(
			@ModelAttribute("activitat") Activitat activitat,
			BindingResult bindingResult, Model model) {
		ActivitatValidator activitatValidator = new ActivitatValidator();
		activitatValidator.validate(activitat, bindingResult);
		if (bindingResult.hasErrors()) {
			//Si no s'afegix una altra vegada desapareix...
			model.addAttribute("llistaTipusActivitat", tipusActivitatDao.getAllNoms());
			model.addAttribute("hores", HoraInici.values());
			model.addAttribute("nivells", Nivell.values());
			return "admin/activitats/add";
		}
		activitatDao.addSerial(activitat);
		model.asMap().clear();
		return "redirect:../activitats/tipus/" + activitat.getTipusActivitat() + ".html";
	}

	// ESBORRAR
	@RequestMapping(value = "/delete/{id}")
	public String processDelete(@PathVariable int id,Model model) {
		model.asMap().clear();
		activitatDao.delete(id);
		return "redirect:../activitats/tipus/" + activitatDao.getTipusActivitat(id) + ".html";
	}
	
	class ActivitatValidator implements Validator {
		@Override
		public boolean supports(Class<?> cls) {
			return Activitat.class.isAssignableFrom(cls);
		}

		@Override
		public void validate(Object obj, Errors errors) {
			Activitat activitat = (Activitat) obj;

			if (activitat.getNom() == null || activitat.getNom() == "") {
				errors.rejectValue("nom", "noNom", "Nom buit");
			}
			
			if (activitat.getDuradaHores() == 0) {
				errors.rejectValue("duradaHores", "noHores", "L'activitat ha de durar mes de 0 hores");
			}
			
			if (activitat.getNivell() == null) {
				errors.rejectValue("nivell", "typeMismatch", "Nivell buit");
			}
			
			if (activitat.getHoraInici() == null) {
				errors.rejectValue("horaInici", "noHoraInici", "Hora inici buida");
			}
			
			if (activitat.getDescripcio() == null || activitat.getDescripcio() == "") {
				errors.rejectValue("descripcio", "noDesripcio", "Descripció buida");
			}
			
			if (activitat.getTipusActivitat() == null || activitat.getTipusActivitat() == "" || activitat.getTipusActivitat() == "none") {
				errors.rejectValue("tipusActivitat", "noTipusActivitat", "Tipus activitat buit");
			}
			
			
			if (activitat.getMinParticipants() < 0) {
				errors.rejectValue("minParticipants", "badParticipants", "Min participants ha de ser superior o igual a 0");
			}
			
			if (activitat.getPreuPerPersona() < 0){
				errors.rejectValue("preuPerPersona", "badPreuPerPersona", "El preu per persona no pot ser negatiu");
			}
			if (activitat.getMaxParticipants() < 0) {
				errors.rejectValue("maxParticipants", "badParticipants", "Max participants ha de ser superior o igual a 0");
			}
			
			if (activitat.getMinParticipants() > activitat.getMaxParticipants()) {
				errors.rejectValue("maxParticipants", "badParticipants", "Max participants ha de ser major o igual que min participants");
			}

		}
	}


}