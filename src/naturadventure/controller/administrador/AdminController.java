package naturadventure.controller.administrador;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import naturadventure.dao.CategoriaDao;
import naturadventure.dao.InfoPersonalDao;
import naturadventure.dao.TipusActivitatDao;
import naturadventure.dao.UsuariDao;
import naturadventure.model.Categoria;
import naturadventure.model.PersonaRegistrada;
import naturadventure.model.TipusActivitat;
import naturadventure.model.Usuari;
import naturadventure.validator.PasswordValidator;
import naturadventure.validator.PerfilValidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/admin")
@Controller
public class AdminController {
	
	@Autowired
	protected TipusActivitatDao tipusActivitatDao;
	@Autowired
	protected CategoriaDao categoriaDao;
	@Autowired
	private UsuariDao usuariDao;
	@Autowired
	private InfoPersonalDao infoPersonalDao;
	
	private final static String DEFAULT_PATH = "admin/reserves/pendents";
	
	@ModelAttribute("categories")
	public List<String> getCategories() {
		return categoriaDao.getAllNoms();
	}
	
	@ModelAttribute("llistaTipusActivitats")
	public ArrayList<List<TipusActivitat>> getCategories(Model model) {
		ArrayList<List<TipusActivitat>> llistaTipusActivitats = new ArrayList<List<TipusActivitat>>();
		for(Categoria c:categoriaDao.getAll())
			llistaTipusActivitats.add(tipusActivitatDao.getByCategoria(c.getNom()));
		return llistaTipusActivitats;
	}
	
	@RequestMapping(value = "/perfil")
	@Deprecated
	/** Reempla�at per la el editor de perfil disponible en la p�gina principal. */
	public String veurePerfil(Model model, HttpSession session) {
		model.addAttribute("personaRegistrada", session.getAttribute("personaRegistrada"));
		return "admin/perfil";
	}

	@RequestMapping(value = "/perfil", method = RequestMethod.POST)
	@Deprecated
	public String guardarPerfil(@ModelAttribute("personaRegistrada") PersonaRegistrada rf,
			BindingResult bindingResult, HttpSession session, Model model) {

		PerfilValidator perfilValidator = new PerfilValidator();
		perfilValidator.validate(rf, bindingResult);
		
		if (bindingResult.hasErrors()) {
			return "admin/perfil";
		}
		
		//Actualitza la informaci� del usuari
		infoPersonalDao.update(rf.getInfoPersonal());
		
		//Actualitzem l'atribut personaRegistrada a la sessi�
		session.setAttribute("personaRegistrada", rf);
		
		// Torna a la pagina principal*/
		return "redirect:" + DEFAULT_PATH + ".html";
	}
	
	@RequestMapping(value = "/canviarContrasenya")
	public String contrasenya(Model model, HttpSession session) {
		model.addAttribute("usuari", ((PersonaRegistrada) session.getAttribute("personaRegistrada")).getUsuari());
		return "admin/canviarContrasenya";
	}

	@RequestMapping(value = "/canviarContrasenya", method = RequestMethod.POST)
	public String modificarContrasenya(@ModelAttribute("usuari") Usuari usuari,
			BindingResult bindingResult, HttpSession session, Model model) {

		PasswordValidator passwordValidator = new PasswordValidator();
		passwordValidator.validate(usuari, bindingResult);
		
		if (bindingResult.getFieldError("password") != null) {
			return "admin/canviarContrasenya";
		}
		
		usuariDao.canviarContrasenya(usuari);

		// Torna a la pagina principal*/
		return "redirect:" + DEFAULT_PATH + ".html";
	}
	
}