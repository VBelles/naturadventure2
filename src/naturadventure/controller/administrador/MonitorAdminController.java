package naturadventure.controller.administrador;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpSession;

import naturadventure.dao.InfoPersonalDao;
import naturadventure.dao.SupervisioDao;
import naturadventure.dao.UsuariDao;
import naturadventure.model.Monitor;
import naturadventure.model.Supervisio;
import naturadventure.model.Usuari;
import naturadventure.model.ennumeration.TipusUsuari;
import naturadventure.validator.MonitorValidator;
import naturadventure.validator.RegisterValidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/admin/monitors")
@Controller
public class MonitorAdminController extends AdminController{
	
	@Autowired
	private UsuariDao usuariDao;
	@Autowired
	private InfoPersonalDao infoPersonalDao;
	@Autowired
	private SupervisioDao supervisioDao;

	// LLISTAR
	@RequestMapping("/list")
	public String getMonitors(HttpSession session, Model model) {
		model.addAttribute("monitors", usuariDao.getPersonaRegistrada(TipusUsuari.MONITOR));
		return "admin/monitors/list";
	}

	// MODIFICAR
	@RequestMapping(value = "/update/{username}", method = RequestMethod.GET)
	public String editMonitor(Model model, @PathVariable String username) {
		Monitor monitor = usuariDao.getMonitor(username);
		
		List<String> restaTipusActivitats = new LinkedList<String>();
		for(String ta: tipusActivitatDao.getAllNoms()){
			if(!monitor.getTipusActivitats().contains(ta))
				restaTipusActivitats.add(ta);
		}
		model.addAttribute("restaTipusActivitats", restaTipusActivitats );
		model.addAttribute("monitor", monitor);
		
		return "admin/monitors/update";
	}

	@RequestMapping(value = "/update/{username}", method = RequestMethod.POST)
	public String processUpdateSubmit(@PathVariable String username, @ModelAttribute("monitor") Monitor monitor,
			BindingResult bindingResult, Model model) {
		
		MonitorValidator userValidator = new MonitorValidator();
		userValidator.validate(monitor, bindingResult);
		
		if (bindingResult.hasErrors()) {
			List<String> restaTipusActivitats;
			if(monitor.getTipusActivitats() == null)
				restaTipusActivitats = tipusActivitatDao.getAllNoms();
			else{
				restaTipusActivitats = new LinkedList<String>();
				for(String ta: tipusActivitatDao.getAllNoms()){
					if(!monitor.getTipusActivitats().contains(ta))
						restaTipusActivitats.add(ta);
				}
			}
			model.addAttribute("restaTipusActivitats", restaTipusActivitats );
			return "admin/monitors/update";
		}
		infoPersonalDao.update(monitor.getInfoPersonal());
		if(monitor.getTipusActivitats()!=null)
			supervisioDao.update(monitor.getUsuari().getUsername(), monitor.getTipusActivitats());
		model.asMap().clear();
		return "redirect:../list.html";
	}

	// AFEGIR
	@RequestMapping(value = "/add")
	public String addMonitor(Model model) {
		model.addAttribute("restaTipusActivitats", tipusActivitatDao.getAllNoms());
		model.addAttribute("monitor", new Monitor());
		return "admin/monitors/add";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String processAddSubmit(@ModelAttribute("monitor") Monitor monitor, BindingResult bindingResult, Model model) {

		RegisterValidator userValidator = new RegisterValidator();
		userValidator.validate(monitor, bindingResult);
		
		if (bindingResult.hasErrors()) {
			if(usuariDao.getByUsername(monitor.getUsuari().getUsername()) != null){
				bindingResult.rejectValue("usuari.username", "baduser", "Usuari no disponible");
			}
			List<String> restaTipusActivitats;
			if(monitor.getTipusActivitats() == null)
				restaTipusActivitats = tipusActivitatDao.getAllNoms();
			else{
				restaTipusActivitats = new LinkedList<String>();
				for(String ta: tipusActivitatDao.getAllNoms()){
					if(!monitor.getTipusActivitats().contains(ta))
						restaTipusActivitats.add(ta);
				}
			}
			model.addAttribute("restaTipusActivitats", restaTipusActivitats );
			return "admin/monitors/add";
		}
		
		//Registra al monitor en el sistema
		int codiInfo = infoPersonalDao.addSerial(monitor.getInfoPersonal());
		monitor.getUsuari().setCodiInfo(codiInfo);
		monitor.getUsuari().setTipusUsuari(TipusUsuari.MONITOR);
		usuariDao.add(monitor.getUsuari());

		if(monitor.getTipusActivitats() != null)
			for(String tipusActivitat : monitor.getTipusActivitats())
				supervisioDao.add(new Supervisio(monitor.getUsuari().getUsername(), tipusActivitat));
		
		model.asMap().clear();
		return "redirect:list.html";
	}

	// ESBORRAR
	@RequestMapping(value = "/delete/{username}")
	public String processDelete(@PathVariable String username, Model model) {
		Usuari usuari = usuariDao.get(username);
		usuariDao.delete(username);
		infoPersonalDao.delete(usuari.getCodiInfo());
		
		model.asMap().clear();
		return "redirect:../list.html";
	}

}
