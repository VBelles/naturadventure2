package naturadventure.controller;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import naturadventure.dao.InfoPersonalDao;
import naturadventure.dao.UsuariDao;
import naturadventure.model.InfoPersonal;
import naturadventure.model.PersonaRegistrada;
import naturadventure.model.Usuari;
import naturadventure.model.ennumeration.TipusUsuari;
import naturadventure.validator.PerfilValidator;
import naturadventure.validator.RegisterValidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controlador per registrar i loguejar als usuaris
 *
 */
@Controller
public class UserController {

	private UsuariDao usuariDao;
	private InfoPersonalDao infoPersonalDao;

	@Autowired
	public void setUsuariDao(UsuariDao usuariDao) {
		this.usuariDao = usuariDao;
	}
	
	@Autowired
	public void setInfoPersonalDao(InfoPersonalDao infoPersonalDao) {
		this.infoPersonalDao = infoPersonalDao;
	}

	@RequestMapping("/login")
	public String login(Model model) {
		model.addAttribute("user", new Usuari());
		return "login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String checkLogin(@ModelAttribute("user") Usuari user,
			BindingResult bindingResult, HttpSession session,
			HttpServletResponse response, HttpServletResponse request) {
		LoginUsuariValidator userValidator = new LoginUsuariValidator();
		userValidator.validate(user, bindingResult);
		if (bindingResult.hasErrors()) {
			return "login";
		}
		
		// Comprova que el login siga correcte
		// intentant carregar les dades de l'usuari
		Usuari newUser = usuariDao.getByUsuari(user.getUsername(), user.getPassword());
		if (newUser == null) {
			bindingResult.rejectValue("password", "badpw",
					"Combinacio d'usuari i contrasenya incorrecta");
			return "login";
		}
		
		// Autenticats correctament.
		// Guardem les dades de l'usuari autenticat a la sessio
		PersonaRegistrada pr = usuariDao.getPersonaRegistrada(user.getUsername());
		session.setAttribute("personaRegistrada", pr);
		session.setAttribute("nomUsuari", pr.getInfoPersonal().getNom());

		String redirect = "redirect:/";
		switch(newUser.getTipusUsuari()){
			case ADMINISTRADOR:
				redirect = "redirect:/admin/reserves/pendents.html";
				break;
			case USUARI:
				redirect = "redirect:/index.html";
				break;
			case MONITOR:
				redirect = "redirect:/monitor/index.html";
				break;
		}

		// Torna a la pagina principal
		return redirect;
	}

	@RequestMapping(value = "/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:/";
	}

	@RequestMapping(value = "/registre")
	public String registrarse(Model model) {
		model.addAttribute("registerForm", new PersonaRegistrada(new Usuari(), new InfoPersonal()));
		return "registre";
	}

	@RequestMapping(value = "/registre", method = RequestMethod.POST)
	public String registrarse(@ModelAttribute("registerForm") PersonaRegistrada rf,
			BindingResult bindingResult, HttpSession session, Model model) {

		RegisterValidator userValidator = new RegisterValidator();
		userValidator.validate(rf, bindingResult);
		
		if (bindingResult.hasErrors()) {
			return "registre";
		}
		
		//Registra l'usuari en el sistema
		int codiInfo = infoPersonalDao.addSerial(rf.getInfoPersonal());
		rf.getUsuari().setCodiInfo(codiInfo);
		rf.getUsuari().setTipusUsuari(TipusUsuari.USUARI);
		usuariDao.add(rf.getUsuari());
		
		
		//Inicia sesió amb el nou usuari
		Usuari user = usuariDao.getByUsuari(rf.getUsuari().getUsername(), rf.getUsuari().getPassword());

		//Error per motiu desconegut
		if (user == null) {
			model.addAttribute("msg", "Error en el registre");
			return "registre";
		}
		model.addAttribute("msg", "Usuari registrat correctament");
		session.setAttribute("user", user); //TODO es necessari afegir aquest atribut ara que usem personaregistrada?
		
		// Torna a la pagina principal*/
		return "redirect:index.html";
	}
	
	
	
	
	
	@RequestMapping(value = "/perfil")
	public String veurePerfil(Model model, HttpSession session) {
		model.addAttribute("personaRegistrada", session.getAttribute("personaRegistrada"));
		return "perfil";
	}

	@RequestMapping(value = "/perfil", method = RequestMethod.POST)
	public String guardarPerfil(@ModelAttribute("personaRegistrada") PersonaRegistrada rf,
			BindingResult bindingResult, HttpSession session, Model model) {

		PerfilValidator perfilValidator = new PerfilValidator();
		perfilValidator.validate(rf, bindingResult);
		
		if (bindingResult.hasErrors()) {
			return "perfil";
		}
		
		//Actualitza la informaci� del usuari
		infoPersonalDao.update(rf.getInfoPersonal());
		
		//Actualitzem l'atribut personaRegistrada a la sessi�
		session.setAttribute("personaRegistrada", rf);
		
		// Torna a la pagina principal*/
		return "redirect:index.html";
	}



	class LoginUsuariValidator implements Validator {
		@Override
		public boolean supports(Class<?> cls) {
			return Usuari.class.isAssignableFrom(cls);
		}

		@Override
		public void validate(Object obj, Errors errors) {
			Usuari user = (Usuari) obj;
			if (user.getUsername() == null || user.getUsername() == "") {
				errors.rejectValue("username", "nouser", "Usuari buit");
			}
			if (user.getPassword() == null || user.getPassword() == "") {
				errors.rejectValue("password", "nopw", "Contrasenya buida");
			}

		}
	}

}