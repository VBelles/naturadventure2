package naturadventure.controller;

import javax.servlet.http.HttpSession;

import naturadventure.dao.ActivitatDao;
import naturadventure.dao.ReservaDao;
import naturadventure.model.PersonaRegistrada;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FrontController extends HeaderController {
	
	@Autowired
	ActivitatDao activitatDao;
	@Autowired
	ReservaDao reservaDao;
	
	@RequestMapping("index")
	/** Obtenim una llista de les activitats més demanades i recents. */
	public String mostrarIndex(Model model) {
		model.addAttribute("demanades", activitatDao.getMesDemanades());
		model.addAttribute("recents", activitatDao.getUltimesDemanades());
		return "index";
	}
	@RequestMapping("reserves")
	public String llistarReserves(HttpSession session, Model model) {
		model.addAttribute("reserves", reservaDao.getByCodiInfo(((PersonaRegistrada) session.getAttribute("personaRegistrada")).getInfoPersonal().getCodi()));
		return "reserves";
	}
	
	@RequestMapping("*")
	public String welcomeFile(Model model) {
		model.addAttribute("demanades", activitatDao.getMesDemanades());
		model.addAttribute("recents", activitatDao.getUltimesDemanades());
	    return "index";
	}
	
}