package naturadventure.controller;

import naturadventure.dao.ActivitatDao;
import naturadventure.model.Activitat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/activitat")
public class ActivitatController extends HeaderController {
	
	@Autowired
	ActivitatDao activitatDao;

	@RequestMapping(value="/{codiActivitat}", method=RequestMethod.GET)
	/** Fem una consulta a la bd per comprovar si existeix dita activitat. Si no retorna error, la passem com a contingut. */
	public String mostrarActivitat(Model model, @PathVariable String codiActivitat) {
		Activitat a = null;
		try {
			a = activitatDao.get(codiActivitat);
		} catch (EmptyResultDataAccessException e) {
			/* No s'ha trobat el registre */
			System.out.println("Activitat " + codiActivitat + " no ha pogut set trobada. Redirigint a l'índex.");
			return "redirect:/index.html";
		}
		
		model.addAttribute("activitat", a);
		return "activitat";
	}

}