package naturadventure.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import naturadventure.dao.ActivitatDao;
import naturadventure.dao.CategoriaDao;
import naturadventure.dao.InfoPersonalDao;
import naturadventure.dao.ReservaDao;
import naturadventure.dao.SupervisioDao;
import naturadventure.dao.TipusActivitatDao;
import naturadventure.dao.UsuariDao;
import naturadventure.dao.sql.CreateTable;
import naturadventure.model.Activitat;
import naturadventure.model.Categoria;
import naturadventure.model.InfoPersonal;
import naturadventure.model.Reserva;
import naturadventure.model.Supervisio;
import naturadventure.model.TipusActivitat;
import naturadventure.model.Usuari;
import naturadventure.model.ennumeration.Estat;
import naturadventure.model.ennumeration.HoraInici;
import naturadventure.model.ennumeration.Nivell;
import naturadventure.model.ennumeration.TipusUsuari;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MantenimentController {
	
	@Autowired
	private CreateTable createTable;

	//Tots els DAOs
	@Autowired
	UsuariDao 			usuariDao;
	@Autowired
	InfoPersonalDao 	infoPersonalDao;
	@Autowired
	CategoriaDao		categoriaDao;
	@Autowired
	TipusActivitatDao 	tipusActivitatDao;
	@Autowired
	SupervisioDao		supervisioDao;
	@Autowired
	ActivitatDao 		activitatDao;
	@Autowired
	ReservaDao			reservaDao;

	
	@RequestMapping("/manteniment")
	public String mostrar(Model model) {
		return "manteniment";
	}
	
	@RequestMapping("/crear_taules")
	public String crearTaules(Model model) {
		createTable.crearEnnumeracions();
		createTable.crearTaules();
		return "index";
	}
	
	@RequestMapping("/borrar_taules")
	public String borrarTaules(Model model) {
		createTable.borrarTaules();
		createTable.borrarEnnumeracions();
		return "index";
	}


	
	@RequestMapping("/inicialitzar_bd")
	public String initBD(Model model) {
		Random ran = new Random();
		System.out.println("INICIALITZANT BD");
		
		List<InfoPersonal> iplist = new ArrayList<InfoPersonal>();
		String[] noms = new String[] {
				"Pepe Garcia", "Alfonso Diago", "Pedro Oliver", "Maria Arriero", "Antonio Bonifas",
				"Jose Bonilla", "Manolo Viruela", "Ivan de la Rosa", "Rodrigo Mendez", "Ana Pardo"};
		String[] emails = new String[] {
				"pepeg@gmail.com", "alfonsod@aol.com", "pedroo@hotmail.com", "mariaa@yahoo.es", "antoniob@gmail.com",
				"joseb@aol.com", "manolov@gmail.com", "ivandlr@hotmail.com", "rodrigom@gmail.com", "anap@gmail.com"};
		String[] tlfs = new String[] {
				"625487985", "689546785", "623548789", "612457845", "632654129",
				"694512348", "645781249", "625431597", "645178457", "623512458"};
		for(int i=0;i<10;i++) {
			InfoPersonal ip = new InfoPersonal();
			ip.setNom(noms[i]);
			ip.setEmail(emails[i]);
			ip.setTelefon(tlfs[i]);
			iplist.add(ip);
			
			infoPersonalDao.addSerial(ip);
			System.out.println("Afegida:\n" + ip.toString());
		}
		
		/*
		 * Inserim a l'admin en la bd
		 */
		
		InfoPersonal ipAdmin = new InfoPersonal();
			ipAdmin.setCodi(1000);
			ipAdmin.setEmail("admin@bd.com");
			ipAdmin.setNom("admin");
			ipAdmin.setTelefon("123456789");
			
		Usuari admin = new Usuari();
			admin.setCodiInfo(1000);
			admin.setUsername("admin");
			admin.setPassword("123456");
			admin.setTipusUsuari(TipusUsuari.ADMINISTRADOR);
		
		infoPersonalDao.add(ipAdmin);
		usuariDao.add(admin);
		
		
		
		ArrayList<Usuari> ulist = new ArrayList<Usuari>();
		for(int i=1;i<=10;i++) {
			Usuari u = new Usuari();
			u.setCodiInfo(i);
			u.setUsername(emails[i-1].split("@")[0]); //la primera parte del email, la tomamos como el nombre de usuario
			u.setPassword("123456");
			if (i % 2 == 0)
				u.setTipusUsuari(TipusUsuari.USUARI);
			else
				u.setTipusUsuari(TipusUsuari.MONITOR);
			ulist.add(u);
			
			usuariDao.add(u);
		}
		
		

		ArrayList<Categoria> clist = new ArrayList<Categoria>();
		String[] cNoms = new String[] {"Aquatics", "Multiaventura", "Muntanya", "Neu"};
		for(int i=0;i<4;i++) {
			Categoria c = new Categoria();
			c.setNom(cNoms[i]);
			clist.add(c);
			
			categoriaDao.add(c);
		}
		
		
		ArrayList<TipusActivitat> talist = new ArrayList<TipusActivitat>();
		String[][] taNoms = new String[][] {
				{"Surf", "Submarinisme", "Busseig amb oxigen"},
				{"Barranquisme", "Tirolina", "Puenting"},
				{"Senderisme", "Espeleologia", "Escalada"},
				{"Esqui", "Snowboarding", "Motos de neu"}};
		for(int i=0;i<4;i++) {
			for(int j=0;j<3;j++) {
				TipusActivitat ta = new TipusActivitat();
				String nomCategoria = cNoms[i];
				String nomTipusActivitat = taNoms[i][j];
				ta.setCategoria(nomCategoria);
				ta.setNom(nomTipusActivitat);
				ta.setDescripcio(nomTipusActivitat + " es una activitat de la categoria " + nomCategoria);
				talist.add(ta);
				
				tipusActivitatDao.add(ta);
			}
		}
		
		
		ArrayList<Supervisio> slist = new ArrayList<Supervisio>();
		for(int i=0;i<10;i+=2) { //Agafem sols els codis dels monitors
			Supervisio s = new Supervisio();
			s.setUsernameMonitor(ulist.get(i).getUsername());
			for(int j=0;j<2;j++) {
				s.setTipusActivitat(talist.get(ran.nextInt(talist.size())).getNom());
				slist.add(s);
				
				//Com el proces es aleatori es pot donar que el mateix monitor se li assigna el mateix tipusactivitat dues voltes. Ignorem els errors.
				try {
					supervisioDao.add(s);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		
		ArrayList<Activitat> alist = new ArrayList<Activitat>();
		String[][][] aNoms = new String[][][] {
				{{"Iniciacio al surf", "Surf per a tota la familia"},
				{"Entre corals", "Submarinisme amb oxigen"},
				{"Busseig de riu", "Avistament de tiburons"}},
				
				{{"Descens pel curs del riu", "Gimcana per la badalona"},
				{"Tirolina infantil Serra Espada", "Descens amb tirolina"},
				{"Puenting de riu", "Iniciacio al puenting"}},
				
				{{"Pujada a Pipa", "Excursio als Pirineus"},
				{"Coves de Altamira", "Exploracio de la cova del Pindal"},
				{"Pujada vertical en Elx", "Descens del pic de Alacant"}},
				
				{{"Esqui per a tota la familia", "Esqui extrem en els Pirineus"},
				{"Snowboarding tot el any en pistes artificials", "Descens en grup"},
				{"Recorregut en moto per la Serra Morena", "Cursa amb motos de neu"}}};
		
		for(int i=0;i<4;i++) {
			for(int j=0;j<3;j++) {
				for(int k=0;k<2;k++) {
					Activitat a = new Activitat();
					String tipusActivitat = taNoms[i][j];
					String nomActivitat = aNoms[i][j][k];
					
					a.setNom(nomActivitat);
					a.setTipusActivitat(tipusActivitat);
					a.setDescripcio(nomActivitat + " es una activitat de tipus " + tipusActivitat);
		
					a.setDuradaHores(ran.nextInt(8) + 2);
					a.setHoraInici(HoraInici.values()[ran.nextInt(3)]);
					a.setMaxParticipants(ran.nextInt(8) + 4);
					a.setMinParticipants(ran.nextInt(3) + 1);
					a.setNivell(Nivell.values()[ran.nextInt(3)]);
					a.setPreuPerPersona(ran.nextInt(6) * 5 + 5);
					
					alist.add(a);
					
					activitatDao.addSerial(a);
				}
			}
		}
		
		
		ArrayList<Reserva> rlist = new ArrayList<Reserva>();
		for(int i=1;i<=10;i++) {
			Reserva r = new Reserva();
			r.setCodiInfo(ran.nextInt(10) + 1); //10 InfoPersonals
			
			int codiActivitat = ran.nextInt(24 + 1); //Tenim 24 activitats en total. Cal afegir un perque començem a contar desde 1.
			r.setCodiActivitat(codiActivitat); 
			
			r.setEstat(Estat.values()[ran.nextInt(3)]);
			r.setHoraInici(HoraInici.values()[ran.nextInt(3)]);
			
			int numParticipants = ran.nextInt(8) + 1; //Min: 1, Max: 8
			r.setNumParticipants(numParticipants);
			int codiActivitatRevisat = (codiActivitat -1 < 1 ? 1 : codiActivitat -1); //Evitem que el codiActivitat que se assigna siga -1.
			r.setPreuFinal(numParticipants * alist.get(codiActivitatRevisat).getPreuPerPersona());
			r.setUsernameMonitor(ulist.get(ran.nextInt(5) * 2).getUsername()); //Monitor aleatori.
			
			//La data de reserva serà instantània, la de activitat dins de una setmana.
			Date dReserva = Calendar.getInstance().getTime();
			Date dActivitat = Calendar.getInstance().getTime();
			dActivitat.setTime(System.currentTimeMillis() + (1000*60*60*24*7));
			
			r.setDataReserva(dReserva);
			r.setDataActivitat(dActivitat);

			rlist.add(r);
			
			reservaDao.addSerial(r);
		}
		
		
		return "index";
	}
	
}