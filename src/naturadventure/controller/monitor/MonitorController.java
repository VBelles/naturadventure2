package naturadventure.controller.monitor;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpSession;

import naturadventure.dao.CategoriaDao;
import naturadventure.dao.InfoPersonalDao;
import naturadventure.dao.TipusActivitatDao;
import naturadventure.dao.ReservaDao;
import naturadventure.dao.UsuariDao;
import naturadventure.model.InfoPersonal;
import naturadventure.model.PersonaRegistrada;
import naturadventure.model.ReservaActivitat;
import naturadventure.model.Usuari;
import naturadventure.validator.PasswordValidator;
import naturadventure.validator.PerfilValidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/monitor")
@Controller
public class MonitorController {
	
	@Autowired
	TipusActivitatDao tipusActivitatDao;
	@Autowired
	CategoriaDao categoriaDao;
	@Autowired
	ReservaDao reservaDao;
	@Autowired
	InfoPersonalDao infoPersonalDao;
	@Autowired
	UsuariDao usuariDao;
	
	@ModelAttribute("diesSetmana")
	public String[] getCategories() {
		return new String[]{"Dilluns", "Dimarts", "Dimécres", "Dijous", "Divendres", "Dissabte", "Diumenge"};
	}
	
	private String[] mesos = {"Gener", "Febrer", "Març", "Abril", "Maig", "Juny",
       	    "Juliol", "Agost", "Setembre", "Octubre",
       	    "Novembre", "Desembre"};
	
	@RequestMapping(value = "/index")
	public String index(Model model){
		Calendar c = Calendar.getInstance();
		int mes = c.get(Calendar.MONTH) + 1;
		int any = c.get(Calendar.YEAR);
		int dia = c.get(Calendar.DAY_OF_MONTH);
		model.asMap().clear();
		return "redirect:/monitor/calendari/"+dia+"/"+mes+"/"+any+".html";	
	}
	
	@RequestMapping(value = "/")
	public String admin(Model model){
		Calendar c = Calendar.getInstance();
		int mes = c.get(Calendar.MONTH) + 1;
		int any = c.get(Calendar.YEAR);
		int dia = c.get(Calendar.DAY_OF_MONTH);
		model.asMap().clear();
		return "redirect:/monitor/calendari/"+dia+"/"+mes+"/"+any+".html";	
	}
	
	@RequestMapping(value = "/list")
	public String llistaActivitats(Model model, HttpSession session){
		String username = ((PersonaRegistrada) session.getAttribute("personaRegistrada")).getUsuari().getUsername();
		model.addAttribute("reservesActivitats", reservaDao.getReservesDelUsuari(username));
		return "monitor/list";
	}
	
	@RequestMapping(value = "/calendari/{dia}/{mes}/{any}", method = RequestMethod.GET)
	public String getEventCalendari(Model model, @PathVariable int dia, @PathVariable int mes, @PathVariable int any, HttpSession session){
		if(mes < 1) mes = 12;
		else if(mes > 12) mes = 1;
		
		Calendar c = new GregorianCalendar(any, mes-1, 0);
		Calendar c2 = new GregorianCalendar(any, mes, 0);
		
		int diaInicial = c.get(Calendar.DAY_OF_WEEK);
		if(diaInicial == 0) diaInicial = 7;	

		String nomMes = mesos[mes-1];
		int diesMes = c2.getActualMaximum(Calendar.DAY_OF_MONTH);
		
		model.addAttribute("diaInicial", diaInicial);
		model.addAttribute("nomMes", nomMes);
		
		if(dia > diesMes) dia = diesMes;
		c.set(any, mes-1, dia);
		
		model.addAttribute("dia", dia);
		model.addAttribute("mes", mes);
		model.addAttribute("any", any);
		model.addAttribute("diesMes", diesMes);
		
		List<Integer> diesEvents = null;
		model.addAttribute("diesEvents", diesEvents);
		
		String username = ((PersonaRegistrada) session.getAttribute("personaRegistrada")).getUsuari().getUsername();
		List<ReservaActivitat> reservesDia = reservaDao.getReservesDelDia(username, c);
		List<Integer> diesMesReservats = reservaDao.getReservesDelMes(username, c);

		model.addAttribute("reservesDia", reservesDia);
		model.addAttribute("reservesMes", diesMesReservats);
		
		return "monitor/index";
	}
	
	@RequestMapping(value = "/perfil")
	public String veurePerfil(Model model, HttpSession session) {
		model.addAttribute("personaRegistrada", session.getAttribute("personaRegistrada"));
		return "monitor/perfil";
	}

	@RequestMapping(value = "/perfil", method = RequestMethod.POST)
	public String guardarPerfil(@ModelAttribute("personaRegistrada") PersonaRegistrada rf,
			BindingResult bindingResult, HttpSession session, Model model) {

		PerfilValidator perfilValidator = new PerfilValidator();
		perfilValidator.validate(rf, bindingResult);
		
		if (bindingResult.hasErrors()) {
			return "monitor/perfil";
		}
		
		//Actualitza la informaci� del usuari
		infoPersonalDao.update(rf.getInfoPersonal());
		
		//Actualitzem l'atribut personaRegistrada a la sessi�
		//session.setAttribute("personaRegistrada", rf);
		
		// Torna a la pagina principal*/
		return "redirect:/monitor/index.html";
	}
	
	@RequestMapping(value = "/canviarContrasenya")
	public String contrasenya(Model model, HttpSession session) {
		model.addAttribute("usuari", ((PersonaRegistrada) session.getAttribute("personaRegistrada")).getUsuari());
		return "monitor/canviarContrasenya";
	}

	@RequestMapping(value = "/canviarContrasenya", method = RequestMethod.POST)
	public String modificarContrasenya(@ModelAttribute("usuari") Usuari usuari,
			BindingResult bindingResult, HttpSession session, Model model) {

		PasswordValidator passwordValidator = new PasswordValidator();
		passwordValidator.validate(usuari, bindingResult);
		
		if (bindingResult.getFieldError("password") != null) {
			return "monitor/canviarContrasenya";
		}
		
		usuariDao.canviarContrasenya(usuari);

		// Torna a la pagina principal*/
		return "redirect:/monitor/index.html";
	}
	
	
	
	
	

}
