package naturadventure.controller;

import java.util.List;

import naturadventure.dao.ActivitatDao;
import naturadventure.model.Activitat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/tipus")
public class TipusActivitatController extends HeaderController {
	
	@Autowired
	ActivitatDao activitatDao;

	
	@RequestMapping(value="/{nomTipusActivitat}", method=RequestMethod.GET)
	/** Fem una consulta a la bd per comprovar si existeix dit tipusActivitat. Si no retorna error, passem com arguments
	 * el nom d'aquesta i les activitats que conté. */
	public String mostrarTipusActivitats2(Model model, @PathVariable String nomTipusActivitat) {
		List<Activitat> activitats = null;
		try {
			activitats = activitatDao.getByTipusActivitat(nomTipusActivitat);
		} catch (EmptyResultDataAccessException e) {
			/* No s'ha trobat el registre */
			System.out.println("TipusActivitat " + nomTipusActivitat +
					" no ha pogut set trobada o no conté activitats. Redirigint a l'índex.");
			return "redirect:/index.html";
		}		
		
		model.addAttribute("nomTipusActivitat", nomTipusActivitat);
		model.addAttribute("activitats", activitats);
		return "tipus";
	}

}