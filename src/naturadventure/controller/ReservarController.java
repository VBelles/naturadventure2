package naturadventure.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.http.HttpSession;

import naturadventure.dao.ActivitatDao;
import naturadventure.dao.InfoPersonalDao;
import naturadventure.dao.ReservaDao;
import naturadventure.mail.GoogleMail;
import naturadventure.model.Activitat;
import naturadventure.model.ClientReserva;
import naturadventure.model.InfoPersonal;
import naturadventure.model.PersonaRegistrada;
import naturadventure.model.Reserva;
import naturadventure.model.ennumeration.Estat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/reserva")
public class ReservarController extends HeaderController {
	
	@Autowired
	InfoPersonalDao infoPersonalDao;
	@Autowired
	ActivitatDao activitatDao;
	@Autowired
	ReservaDao reservaDao;
	
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }

	
	@RequestMapping(value="/{codiActivitat}", method=RequestMethod.GET)
	/** Fem una consulta a la bd per comprovar si existeix dita activitat. Si no retorna error, la passem com a contingut. */
	public String reservar(Model model, @PathVariable String codiActivitat, HttpSession session) {
		Activitat a = null;
		try {
			a = activitatDao.get(codiActivitat);
		} catch (EmptyResultDataAccessException e) {
			/* No s'ha trobat el registre */
			System.out.println("Activitat " + codiActivitat + " no ha pogut set trobada. Redirigint a l'index.");
			return "redirect:/index.html";
		}
		
		PersonaRegistrada pr = (PersonaRegistrada) session.getAttribute("personaRegistrada");
		if(pr != null){
			model.addAttribute("clientReserva", new ClientReserva(pr.getInfoPersonal(), new Reserva()));
		}
		else{
			model.addAttribute("clientReserva", new ClientReserva(new InfoPersonal(), new Reserva()));
		}
		model.addAttribute("activitat", a);
		return "reserva";
	}
	
	@RequestMapping(value="/{codiActivitat}", method=RequestMethod.POST)
	public String reservarSubmitProcess(@PathVariable String codiActivitat,
			@ModelAttribute("clientReserva") ClientReserva clientReserva,
			BindingResult bindingResult, HttpSession session, Model model) {
		
		//Indiquem el codi d'activitat de la reserva abans de validar-ho
		clientReserva.getReserva().setCodiActivitat(Integer.parseInt(codiActivitat));
		int codiInfo = -1;
		

		if(session.getAttribute("personaRegistrada")!=null) {
			PersonaRegistrada pr = (PersonaRegistrada) session.getAttribute("personaRegistrada");
			clientReserva.setInfoPersonal(pr.getInfoPersonal());
			codiInfo = pr.getInfoPersonal().getCodi();
		}
		
		ReservaValidator reservaValidator = new ReservaValidator();
		reservaValidator.validate(clientReserva, bindingResult);
		
		
		Activitat a = activitatDao.get(codiActivitat);
		
		if (bindingResult.hasErrors()) {
			System.out.println("Se han trobat errors.");
			model.addAttribute("activitat", a);
			return "reserva";
		}
		
	
		Reserva r = clientReserva.getReserva();
		InfoPersonal ip = clientReserva.getInfoPersonal();
		
		//Encara que el preu final es calcule mitjan�ant js a la reserva, el obtenim directament d'aquesta manera.
		r.setPreuFinal(r.getNumParticipants() * a.getPreuPerPersona());
		

		//Comprovem si cal afegir la infoPersonal a la bd, en cas de que haja reservat un usuari no registrat 
		if(session.getAttribute("personaRegistrada") == null){
			codiInfo = infoPersonalDao.addSerial(ip);
		}

		r.setCodiInfo(codiInfo);
		
		//Agafem l'hora d'inici de l'activitat per si aquesta canviara en el futur
		r.setHoraInici(a.getHoraInici());
		
		//Afegim la resta de parametres
		r.setEstat(Estat.PENDENT);
		r.setDataReserva(Calendar.getInstance().getTime()); //Hora actual
		r.setUsernameMonitor(null);
		

		
		//Enviem el e-mail al client
		Date data = r.getDataActivitat();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String simbolEuro = "&#8364;";
		
		String assumpte = "Reservada l'activitat " + a.getNom();
		String missatge = "<p>S'ha enregistrat la reserva de l'activitat " + a.getNom() + " el " +
				dateFormat.format(data) + " per a " +r.getNumParticipants() + " persones." +
				"El preu total es de " + r.getPreuFinal() + simbolEuro + "." + "</p>" +
				"<p>Pronte ens posarem amb contacte amb tu per tal de confirmar la teva reserva.</p>" +
				"<p>Gr�cies per confiar en NaturAdventure.</p>";
		
		try {
			GoogleMail.Send(ip.getEmail(), assumpte, missatge);
		} catch (AddressException e) {
			//Si es produeix un error amb l'adressa, es probablement perque la
			//direccio es falsa, i com a mesura no plenem la reserva ni guardem cap informacio.
			System.out.println("Address exception!" + e.getMessage() + e.getStackTrace());
			if (ip != null) {
				infoPersonalDao.delete(ip);
			}
		} catch (MessagingException e) {
			System.out.println("Messaging exception!" + e.getMessage() + e.getStackTrace());
			e.printStackTrace();
		}
		
		//Si tot ha anat be, registrem la reserva
		reservaDao.addSerial(r);
		
		return "redirect:/index.html";
	}
	

	

	class ReservaValidator implements Validator {

		@Override
		public boolean supports(Class<?> cls) {
			return ClientReserva.class.isAssignableFrom(cls);
		}

		@Override
		public void validate(Object obj, Errors errors) {
			ClientReserva cr = (ClientReserva) obj;
			
			Reserva r = cr.getReserva();
			Date dataActivitat = r.getDataActivitat();
			if (dataActivitat == null) {
				errors.rejectValue("reserva.dataActivitat", "nullactivitat", "Cal indicar una data de reserva");
			} else if (dataActivitat.before(Calendar.getInstance().getTime())) {
				errors.rejectValue("reserva.dataActivitat", "pastdate", "No es pot reservar en una data que ja ha passat");
			}
			
			Activitat a = activitatDao.get(r.getCodiActivitat());
			int numParticipants = r.getNumParticipants();
			if (numParticipants == 0) {
				errors.rejectValue("reserva.numParticipants", "nullparticipants", "Cal seleccionar la quantitat de participants");
			} else if (numParticipants < a.getMinParticipants() || numParticipants > a.getMaxParticipants()) {
				errors.rejectValue("reserva.numParticipants", "participants", "La quantitat de participants deu estar dins dels l�mits");
			}
			
			InfoPersonal ip = cr.getInfoPersonal();
			if (ip.getNom() == null || ip.getNom() == "") {
				errors.rejectValue("infoPersonal.nom", "nonom", "El nom no pot estar buit");
			}
			
			if (ip.getTelefon() == null || ip.getTelefon() == "") {
				errors.rejectValue("infoPersonal.telefon", "notelefon", "El telefon no pot estar buit");
			} else if(!isValidPhoneNumber(ip.getTelefon())){
				errors.rejectValue("infoPersonal.telefon", "badtelefon", "Este telefon no es valid");
			}
			
			if (ip.getEmail() == null || ip.getEmail() == "") {
				errors.rejectValue("infoPersonal.email", "noemail", "El email no pot estar buit");
			} else if (!isValidEmail(ip.getEmail())) {
				errors.rejectValue("infoPersonal.email", "bademail", "Este email no es valid");
			}
		}
		
		public boolean isValidPhoneNumber(String number) {
			if(number.length() > 12) return false;
			String pattern = "^([0-9]+){9,12}$";
			java.util.regex.Pattern p = java.util.regex.Pattern.compile(pattern);
			java.util.regex.Matcher m = p.matcher(number);
			return m.matches();
		}
		
		private boolean isValidEmail(String email) {
			String pattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
			java.util.regex.Pattern p = java.util.regex.Pattern.compile(pattern);
			java.util.regex.Matcher m = p.matcher(email);
			return m.matches();
		}
	}
	
}