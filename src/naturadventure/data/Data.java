package naturadventure.data;


public class Data  {

	private int dia;
	private int mes;
	private int any;
	
	/**
	 * Data en format dd/mm/aaaa
	 * @param data
	 */
	public Data(String data){
		String[] fields;
		try{
			fields = data.split("/");
			dia = (Integer.parseInt(fields[0]));
			mes = (Integer.parseInt(fields[1]));
			any = (Integer.parseInt(fields[2]));
		}catch(Exception e){
			
		}
	}
	
	@Override
	public String toString(){
		return dia + "/" + mes + "/" + any;
	}
	

}
