package naturadventure.validator;

import naturadventure.model.InfoPersonal;
import naturadventure.model.PersonaRegistrada;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class PerfilValidator implements Validator {

	@Override
	public boolean supports(Class<?> cls) {
		return InfoPersonal.class.isAssignableFrom(cls);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		PersonaRegistrada rf = (PersonaRegistrada) obj;
		
		InfoPersonal ip = rf.getInfoPersonal();
		if (ip.getTelefon() == null || ip.getTelefon() == "") {
			errors.rejectValue("infoPersonal.telefon", "notelefon", "El telefon no pot estar buit");
		}

		else if(!isValidPhoneNumber(ip.getTelefon())){
			errors.rejectValue("infoPersonal.telefon", "badtelefon", "Este telefon no es valid");
		}

		if (ip.getEmail() == null || ip.getEmail() == "") {
			errors.rejectValue("infoPersonal.email", "noemail", "El email no pot estar buit");
		}
		else if (!isValidEmail(ip.getEmail())) {
			errors.rejectValue("infoPersonal.email", "bademail", "Este email no es valid");
		}

	}
	
	public boolean isValidPhoneNumber(String number) {
		String pattern = "^([0-9]+){9}$";
		
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(pattern);
		java.util.regex.Matcher m = p.matcher(number);
		return m.matches();
	}
	
	private boolean isValidEmail(String email) {
		String pattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(pattern);
		java.util.regex.Matcher m = p.matcher(email);
		return m.matches();
	}
}