package naturadventure.validator;

import java.util.Calendar;
import java.util.Date;

import naturadventure.dao.ActivitatDao;
import naturadventure.model.Activitat;
import naturadventure.model.ClientReserva;
import naturadventure.model.InfoPersonal;
import naturadventure.model.Reserva;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class ReservaValidator implements Validator {
	
	@Autowired
	ActivitatDao activitatDao;

	@Override
	public boolean supports(Class<?> cls) {
		return ClientReserva.class.isAssignableFrom(cls);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		ClientReserva cr = (ClientReserva) obj;
		
		Reserva r = cr.getReserva();
		Date dataActivitat = r.getDataActivitat();
			
		if (dataActivitat == null) {
			errors.rejectValue("reserva.dataActivitat", "nullactivitat", "Cal indicar una data de reserva");
		} else if (dataActivitat.before(Calendar.getInstance().getTime())) {
			errors.rejectValue("reserva.dataActivitat", "pastdate", "No es pot reservar en una data que ja ha passat");
		}
		
		System.out.println("2:" + r.getCodiActivitat());
		System.out.println(activitatDao);
		Activitat a = activitatDao.get(r.getCodiActivitat());
		int numParticipants = r.getNumParticipants();
		if (numParticipants == 0) {
			errors.rejectValue("reserva.numParticipants", "nullparticipants", "Cal seleccionar la quantitat de participants");
		} else if (numParticipants < a.getMinParticipants() || numParticipants > a.getMaxParticipants()) {
			errors.rejectValue("reserva.numParticipants", "participants", "La quantitat de participants deu estar dins dels l�mits");
		}
		
		InfoPersonal ip = cr.getInfoPersonal();
		if (ip.getTelefon() == null || ip.getTelefon() == "") {
			errors.rejectValue("infoPersonal.telefon", "notelefon", "El telefon no pot estar buit");
		} else if(!isValidPhoneNumber(ip.getTelefon())){
			errors.rejectValue("infoPersonal.telefon", "badtelefon", "Este telefon no es valid");
		}
		
		if (ip.getEmail() == null || ip.getEmail() == "") {
			errors.rejectValue("infoPersonal.email", "noemail", "El email no pot estar buit");
		} else if (!isValidEmail(ip.getEmail())) {
			errors.rejectValue("infoPersonal.email", "bademail", "Este email no es valid");
		}
	}
	
	public boolean isValidPhoneNumber(String number) {
		String pattern = "^([0-9]+){9}$";
		
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(pattern);
		java.util.regex.Matcher m = p.matcher(number);
		return m.matches();
	}
	
	private boolean isValidEmail(String email) {
		String pattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(pattern);
		java.util.regex.Matcher m = p.matcher(email);
		return m.matches();
	}
}