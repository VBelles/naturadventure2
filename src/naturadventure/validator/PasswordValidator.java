package naturadventure.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import naturadventure.model.Usuari;

public class PasswordValidator implements Validator {
		private static final int MIN_LENGTH_PASSWORD = 5;
		private static final int MAX_LENGTH_PASSWORD = 25;

		@Override
		public boolean supports(Class<?> cls) {
			return Usuari.class.isAssignableFrom(cls);
		}

		@Override
		public void validate(Object obj, Errors errors) {
			Usuari user = (Usuari) obj;

			if (user.getPassword() == null || user.getPassword() == "") {
				errors.rejectValue("password", "badpw", "Contrasenya buida");
			}
			else if (user.getPassword().length() < MIN_LENGTH_PASSWORD || user.getPassword().length() > MAX_LENGTH_PASSWORD) {
				errors.rejectValue("password", "badpw", "La contrasenya ha de ser de entre " + MIN_LENGTH_PASSWORD + " i " + MAX_LENGTH_PASSWORD + " caracters");
			}
		
		}


	}