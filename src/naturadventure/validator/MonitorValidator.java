package naturadventure.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import naturadventure.model.InfoPersonal;
import naturadventure.model.Monitor;
import naturadventure.model.PersonaRegistrada;
import naturadventure.model.Usuari;

public class MonitorValidator implements Validator {
	private static final int MIN_LENGTH_PASSWORD = 5;
	private static final int MAX_LENGTH_PASSWORD = 25;
	private static final int MIN_LENGTH_USERNAME = 5;
	private static final int MAX_LENGTH_USERNAME = 25;

	@Override
	public boolean supports(Class<?> cls) {
		return Usuari.class.isAssignableFrom(cls);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		Monitor monitor = (Monitor) obj;
		InfoPersonal infoPersonal = monitor.getInfoPersonal();

		if (infoPersonal.getNom() == null || infoPersonal.getNom() == "") {
			errors.rejectValue("infoPersonal.nom", "nonom", "El nom no pot estar buit");
		}

		if (infoPersonal.getTelefon() == null || infoPersonal.getTelefon() == "") {
			errors.rejectValue("infoPersonal.telefon", "notelefon", "El telefon no pot estar buit");
		}

		else if(!isValidPhoneNumber(infoPersonal.getTelefon())){
			errors.rejectValue("infoPersonal.telefon", "badtelefon", "Este telefon no es valid");
		}

		if (infoPersonal.getEmail() == null || infoPersonal.getEmail() == "") {
			errors.rejectValue("infoPersonal.email", "noemail", "El email no pot estar buit");
		}
		else if (!isValidEmail(infoPersonal.getEmail())) {
			errors.rejectValue("infoPersonal.email", "bademail", "Este email no es valid");
		}

	}
	
	public boolean isValidPhoneNumber(String number) {
		
		String pattern = "^([0-9]+){9}$";
		
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(pattern);
		java.util.regex.Matcher m = p.matcher(number);
		return m.matches();
	}
	
	public boolean isValidUsername(String username) {
		String pattern = "^[a-zA-Z0-9_.-]*$";
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(pattern);
		java.util.regex.Matcher m = p.matcher(username);
		return m.matches();
	}

	private boolean isValidEmail(String email) {
		String pattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(pattern);
		java.util.regex.Matcher m = p.matcher(email);
		return m.matches();
	}
}