package naturadventure.filter;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import naturadventure.model.PersonaRegistrada;
import naturadventure.model.Usuari;
import naturadventure.model.ennumeration.TipusUsuari;

public class LoginAdministradorFilter implements Filter {

    /**
     * @see Filter#init(FilterConfig)
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}


    
    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);
        PersonaRegistrada user = (session != null) ? (PersonaRegistrada) session.getAttribute("personaRegistrada") : null;
        String loginURL = request.getContextPath() + "/login.html"; 

        if (user == null && !request.getRequestURI().equals(loginURL)) {       
            response.sendRedirect(loginURL);
            return;
        }
        
        if (user.getUsuari().getTipusUsuari().equals(TipusUsuari.USUARI)){
        	String indexUrl = request.getContextPath() + "/index.html"; 
        	response.sendRedirect(indexUrl);
            return;
        }
        
        if (user.getUsuari().getTipusUsuari().equals(TipusUsuari.MONITOR)){
        	String indexUrl = request.getContextPath() + "/monitor/index.html"; 
        	response.sendRedirect(indexUrl);
            return;
        }
        
        chain.doFilter(request, response);
        return;


    }


    /**
     * @see Filter#destroy()
     */
    @Override
    public void destroy() {}

}