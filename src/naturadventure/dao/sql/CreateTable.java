package naturadventure.dao.sql;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
 
public class CreateTable {
	
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
	    jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	//Ennumeracions
	private static final String horaInici = "CREATE TYPE horaInici AS ENUM ('mati', 'vesprada', 'nit');";
	private static final String nivell = "CREATE TYPE nivell AS ENUM ('iniciacio', 'mitja', 'expert');";
	private static final String estat = "CREATE TYPE estat AS ENUM ('pendent', 'acceptada', 'rebutjada');";
	private static final String tipusUsuari = "CREATE TYPE tipusUsuari AS ENUM ('usuari', 'monitor', 'administrador');";
	
	//Taules
	private static final String infoPersonal = "CREATE TABLE InfoPersonal (" +
		  	"codi SERIAL," +
	        " nom VARCHAR(50) NOT NULL," +
			" email VARCHAR(50) NOT NULL," +
		  	" telefon VARCHAR(12)," +
	        " CONSTRAINT cp_infoPersonal PRIMARY KEY (codi));";
	
	private static final String usuari = "CREATE TABLE Usuari (" +
		  	"username VARCHAR(20)," +
			" password VARCHAR(32) NOT NULL," +
			" codiInfo INTEGER NOT NULL," +
			" tipusUsuari TipusUsuari DEFAULT 'usuari'," +
	        " CONSTRAINT cp_usuari PRIMARY KEY (username)," +
	        " CONSTRAINT ca_codiInfo FOREIGN KEY (codiInfo) REFERENCES InfoPersonal(codi) ON DELETE RESTRICT ON UPDATE CASCADE);";
	
	private static final String categoria = "CREATE TABLE Categoria (" +
			"nom VARCHAR(30)," +
			" CONSTRAINT cp_categoria PRIMARY KEY (nom))";
	
	private static final String tipusActivitat = "CREATE TABLE TipusActivitat (" +
			"nom VARCHAR(30)," +
			" categoria VARCHAR(30)," +
			" descripcio VARCHAR(200)," +
			" CONSTRAINT cp_TipusActivitat PRIMARY KEY (nom)," +
			" CONSTRAINT ca_categoria FOREIGN KEY (categoria) REFERENCES Categoria(nom) ON DELETE SET NULL ON UPDATE CASCADE);";
	
	private static final String activitat = "CREATE TABLE Activitat (" +
			"codi SERIAL," +
			" tipusActivitat VARCHAR(30) NOT NULL," +
			" nom VARCHAR(50) NOT NULL," +
			" duradaHores INTEGER," +
			" horaInici HoraInici NOT NULL," +
			" descripcio VARCHAR(2000)," +
			" nivell Nivell NOT NULL," +
			" preuPerPersona REAL," +
			" minParticipants INTEGER," +
			" maxParticipants INTEGER," +
			" CONSTRAINT cp_activitat PRIMARY KEY (codi)," +
			" CONSTRAINT ca_tipusActivitat FOREIGN KEY (tipusActivitat) REFERENCES TipusActivitat(nom) ON DELETE SET NULL ON UPDATE CASCADE);"; 
	
	private static final String supervisio = "CREATE TABLE Supervisio (" +
			"tipusActivitat VARCHAR(30)," +
			" usernameMonitor VARCHAR(20)," +
			" CONSTRAINT cp_monitorActivitat PRIMARY KEY (tipusActivitat, usernameMonitor)," +
			" CONSTRAINT ca_usernameMonitor FOREIGN KEY (usernameMonitor) REFERENCES Usuari(username) ON DELETE CASCADE ON UPDATE CASCADE," + 
			" CONSTRAINT ca_tipusActivitat FOREIGN KEY (tipusActivitat) REFERENCES TipusActivitat(nom) ON DELETE CASCADE ON UPDATE CASCADE);";
	
	private static final String reserva = "CREATE TABLE Reserva (" +
			"codi SERIAL," +
			" codiActivitat INTEGER NOT NULL," +
			" usernameMonitor VARCHAR(20) DEFAULT NULL," +
			" codiInfo INTEGER NOT NULL," +
			" horaInici HoraInici NOT NULL," +
			" estat Estat NOT NULL," +
			" numParticipants INTEGER NOT NULL," +
			" preuFinal REAL NOT NULL," +
			" dataReserva DATE NOT NULL," +
			" dataActivitat DATE NOT NULL," +
			" CONSTRAINT cp_reserva PRIMARY KEY (codi)," +
			" CONSTRAINT ca_usernameMonitor FOREIGN KEY (usernameMonitor) REFERENCES Usuari(username) ON DELETE SET NULL ON UPDATE CASCADE," + 
			" CONSTRAINT ca_codiInfo FOREIGN KEY (codiInfo) REFERENCES InfoPersonal(codi) ON DELETE SET NULL ON UPDATE CASCADE," + 
			" CONSTRAINT ca_codiActividad FOREIGN KEY (codiActivitat) REFERENCES Activitat(codi) ON DELETE SET NULL ON UPDATE CASCADE);";
	
	/*
	 * 
	 */
	
	public void crearTaules() {
		crearTaula("InfoPersonal", 		infoPersonal);
		crearTaula("Usuari", 			usuari);
		crearTaula("Categoria", 		categoria);
		crearTaula("TipusActivitat",	tipusActivitat);
		crearTaula("Activitat", 		activitat);
		crearTaula("Supervisio", 		supervisio);
		crearTaula("Reserva", 			reserva);
	}
	
	public void borrarTaules() {
		borrarTaula("Reserva");
		borrarTaula("Supervisio");
		borrarTaula("Activitat");
		borrarTaula("TipusActivitat");
		borrarTaula("Categoria");
		borrarTaula("Usuari");
		borrarTaula("InfoPersonal");
	}
	
	public void crearEnnumeracions () {
		crearEnnumeracio("Estat", 			estat);
		crearEnnumeracio("Nivell", 			nivell);
		crearEnnumeracio("HoraInici", 		horaInici);
		crearEnnumeracio("TipusUsuari", 	tipusUsuari);
	}
	
	public void borrarEnnumeracions () {
		borrarEnnumeracio("Estat");
		borrarEnnumeracio("Nivell");
		borrarEnnumeracio("HoraInici");
		borrarEnnumeracio("TipusUsuari");
	}
			
	/* Quan intentem borrar una taula el proc�s pot fallar perqu� aquesta no existeix.
	 * Per evitar que el proc�s pare quan es troba un d'aquestos problemes cal incloure
	 * cada execute en el seu propi try-catch. */
	
	private void crearTaula(String nom, String sql) {
		try {
		  	jdbcTemplate.execute(sql);
			System.out.println("Taula " + nom + " creada...");
		} catch (DataAccessException e) {
			System.out.println("Taula " + nom + " no ha pugut ser creada.");
		}
	}
	
	private void borrarTaula(String nom) {
		try {
			jdbcTemplate.execute("DROP TABLE " + nom + ";");
			System.out.println("La taula " + nom + " ha sigut borrada amb �xit.");
		} catch (DataAccessException e) {
			System.out.println("La taula " + nom + " no ha pogut ser borrada.");
		}
	}
	
	private void crearEnnumeracio(String nom, String sql) {
		try {
			jdbcTemplate.execute(sql);
			System.out.println("Enumeraci� " + nom + " creada...");
		} catch (DataAccessException e) {
			System.out.println("Ennumeraci� " + nom + " no ha pugut ser creada.");
		}
	}
	
	private void borrarEnnumeracio(String nom) {
		try {
			jdbcTemplate.execute("DROP TYPE " + nom + ";");
			System.out.println("Enumeraci� " + nom + " borrada...");
		} catch (DataAccessException e) {
			System.out.println("Ennumeraci� " + nom + " no ha pugut ser borrada.");
		}
	}

}