package naturadventure.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import naturadventure.dao.core.DaoInterface;
import naturadventure.dao.core.GenericDao;
import naturadventure.model.InfoPersonal;
import naturadventure.model.Monitor;
import naturadventure.model.PersonaRegistrada;
import naturadventure.model.Reserva;
import naturadventure.model.Supervisio;
import naturadventure.model.TipusActivitat;
import naturadventure.model.Usuari;
import naturadventure.model.ennumeration.TipusUsuari;

import org.jasypt.util.password.BasicPasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class UsuariDao extends GenericDao<Usuari> implements DaoInterface<Usuari> {
	
	@Autowired
	InfoPersonalDao infoPersonalDao;

	@Override
	public String getTable() {
		return "Usuari";
	}
	@Override
	public String[] getPrimaryKeys() {
		return new String[] { "username" };
	}
	@Override
	public Object[] getPrimaryKeys(Usuari u) {
		return new Object[] { u.getUsername() };
	}
	@Override
	public String[] getParams() {
		return new String[] {"password", "codiInfo", "tipusUsuari"};
	}
	@Override
	public Map<String, Object> getParams(Usuari u) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("password", u.getPassword());
		params.put("codiInfo", u.getCodiInfo());
		params.put("tipusUsuari", u.getTipusUsuari());
		return params;
	}
	
	@Override
	public RowMapper<Usuari> getRowMapper() {
		return new UsuariMapper();
	}
	
	private static final class UsuariMapper implements RowMapper<Usuari> {

		public Usuari mapRow(ResultSet rs, int rowNum) throws SQLException {
			Usuari u = new Usuari();
			u.setUsername(rs.getString("username"));
			u.setPassword(rs.getString("password"));
			u.setCodiInfo(rs.getInt("codiInfo"));
			u.setTipusUsuari(TipusUsuari.fromString(rs.getString("tipusUsuari")));
			return u;
		}
	}
	
	private static final class PersonaRegistradaMapper implements RowMapper<PersonaRegistrada> {

		public PersonaRegistrada mapRow(ResultSet rs, int rowNum) throws SQLException {
			Usuari u = new Usuari();
			u.setUsername(rs.getString("username"));
			u.setPassword(rs.getString("password"));
			u.setCodiInfo(rs.getInt("codiInfo"));
			u.setTipusUsuari(TipusUsuari.fromString(rs.getString("tipusUsuari")));
			
			InfoPersonal ip = new InfoPersonal();
			ip.setCodi(rs.getInt("codi"));
			ip.setNom(rs.getString("nom"));
			ip.setEmail(rs.getString("email"));
			ip.setTelefon(rs.getString("telefon"));
			
			return new PersonaRegistrada(u, ip);
		}
	}
	
	
	/** @return un mapa que cont� tota la informaci� continguda en les taules
	 * Usuari i InfoPersonal de un determinat usuari. */
	public Map<String, Object> getAllInfo(String username) {
		String sql = "SELECT * FROM Usuari INNER JOIN InfoPersonal ON (Usuari.codiInfo = InfoPersonal.codi)"
				+ " WHERE Usuari.username = '" + username +"'";
		Map<String, Object> map = jdbcTemplate.queryForMap(sql);
		for(String key:map.keySet())
			System.out.println("key:" + key + " val:" + map.get(key));
		return map;
	}
	
	
	/** @return una llista de tots els usuaris de un determinat tipus. */
	public List<Usuari> getByTipusUsuari(TipusUsuari t) {
		String sql = "SELECT * FROM Usuari WHERE tipusUsuari='" +t.toString() +"'";
		List<Usuari> usuaris = jdbcTemplate.query(sql, getRowMapper());
		return usuaris;
	}
	
	/** @return la InfoPersonal de un determinat usuari. */
	public InfoPersonal getInfoPersonal(String username) {
		String sql = "SELECT InfoPersonal.*"
				+ " FROM Usuari INNER JOIN InfoPersonal ON (Usuari.codiInfo = InfoPersonal.codi)"
				+ " WHERE Usuari.username='" + username + "'";
		return jdbcTemplate.queryForObject(sql, infoPersonalDao.getRowMapper());
	}
	
	/** @return una llista amb les supervisions de un determinat usuari que siga monitor. */
	public List<Supervisio> getSupervisions(String username) {
		String sql = "SELECT Supervisio.*"
				+ " FROM Usuari INNER JOIN Supervisio ON (Usuari.username = Supervisio.usernameMonitor)"
				+ " WHERE Usuari.username='" + username + "' AND Usuari.tipusUsuari='" + TipusUsuari.MONITOR + "'";
		return jdbcTemplate.queryForList(sql, Supervisio.class);
	}
	
	/** @return una llista dels tipus d'activitat que pot supervisar. */
	public List<TipusActivitat> getTipusActivitats(String username) {
		String sql = "SELECT TipusActivitat.*"
				+ " FROM Usuari INNER JOIN Supervisio ON (Usuari.username = Supervisio.usernameMonitor)"
				+ " FROM Supervisio INNER JOIN TipusActivitat ON (Supervisio.tipusActivitat = TipusActivitat.nom)"
				+ " WHERE Usuari.username='" + username + "' AND Usuari.tipusUsuari='" + TipusUsuari.MONITOR + "'";
		return jdbcTemplate.queryForList(sql, TipusActivitat.class);
	}
	
	/** @return una llista amb les reserves amb les quals un usuari est� involucrat com a usuari.
	 * �s a dir, retorna les reserves que ell mateixa ha iniciat. */
	public List<Reserva> getReservesComUsuari(String username) {
		String sql = "SELECT Reserva.*"
				+ " FROM Usuari INNER JOIN Reserva ON (Usuari.codiInfo = Reserva.codiInfo)"
				+ " WHERE Usuari.username='" + username;
		return jdbcTemplate.queryForList(sql, Reserva.class);
	}
	
	/** @return una llista amb les reserves amb les quals un usuari est� involucrat com a monitor.
	 * �s a dir, retorna les reserves en les cuals ha fet de monitor. */
	public List<Reserva> getReservesComMonitor(String username) {
		String sql = "SELECT Reserva.*"
				+ " FROM Usuari INNER JOIN Reserva ON (Usuari.username = Reserva.usernameMonitor)"
				+ " WHERE Usuari.username='" + username + "' AND Usuari.tipusUsuari='" + TipusUsuari.MONITOR + "'";
		return jdbcTemplate.queryForList(sql, Reserva.class);
	}
	
	/** @return Una persona registrada en el sitema amb un usuari especificat*/
	public PersonaRegistrada getPersonaRegistrada(String username){
		PersonaRegistrada pr = new PersonaRegistrada();
		pr.setInfoPersonal(getInfoPersonal(username));
		pr.setUsuari(get(username));
		return pr;
	}
	
	/** @return Una llista de persones registrades en el sistema del tipus especificat*/
	public List<PersonaRegistrada> getPersonaRegistrada(TipusUsuari tipusUsuari){
		String sql = "SELECT Usuari.*, InfoPersonal.*"
				+ " FROM Usuari INNER JOIN InfoPersonal ON (Usuari.codiInfo = InfoPersonal.codi)"
				+ " WHERE Usuari.tipusUsuari='" + tipusUsuari + "'";
		
		return jdbcTemplate.query(sql, new PersonaRegistradaMapper());
	}
	
	

	
	public List<PersonaRegistrada> getMonitors(String tipusActivitat) {
		/*TipusUsuari tipusUsuari = TipusUsuari.MONITOR;
		String sql = "SELECT Usuari.*, InfoPersonal.*"
				+ " FROM InfoPersonal "
				+ " INNER JOIN Usuari ON (Usuari.codiInfo = InfoPersonal.codi)"
				+ " INNER JOIN Supervisio ON (Usuari.username = Supervisio.usernameMonitor)"
				+ " WHERE Usuari.tipusUsuari='" + tipusUsuari + "'"
				+ " AND Supervisio.tipusActivitat='" + tipusActivitat + "'";*/
		
		String sql2 = "SELECT Usuari.*, InfoPersonal.*"
				+ " FROM InfoPersonal "
				+ " INNER JOIN Usuari ON (Usuari.codiInfo = InfoPersonal.codi)"
				+ " WHERE Usuari.username IN"
				+ " 	(SELECT usernameMonitor "
				+ " 	FROM Supervisio "
				+ " 	WHERE tipusActivitat='" + tipusActivitat + "')";
		
		return jdbcTemplate.query(sql2, new PersonaRegistradaMapper());
		
	}
	
	public Monitor getMonitor(String username){
		PersonaRegistrada pr = getPersonaRegistrada(username);
		Monitor monitor = new Monitor(pr.getUsuari(), pr.getInfoPersonal());
		String sql = "SELECT TipusActivitat.nom "
				+ "FROM TipusActivitat INNER JOIN Supervisio ON (TipusActivitat.nom = Supervisio.tipusActivitat)"
				+ "WHERE Supervisio.usernameMonitor='" +username + "'";
		
		monitor.setTipusActivitats(jdbcTemplate.queryForList(sql, String.class));
		
		return monitor;
	}
	

	
	@Override
	public int add(Usuari u) {
		String encryptedPassword = new BasicPasswordEncryptor().encryptPassword(u.getPassword());
		u.setPassword(encryptedPassword);
		return super.add(u);
	}
	
	@Override
	public int addSerial(Usuari u) {
		String encryptedPassword = new BasicPasswordEncryptor().encryptPassword(u.getPassword());
		u.setPassword(encryptedPassword);
		return super.addSerial(u);
	}

	public Usuari getByUsuari(String username, String password) {
		BasicPasswordEncryptor passwordEncryptor = new BasicPasswordEncryptor();
		Usuari user;
		try {
			user = getByUsername(username);
		} catch (Exception e) {
			return null;
		}
		//return user;
		if (passwordEncryptor.checkPassword(password, user.getPassword())) {
			return user;
		} else {
			return null; // bad login!
		}
	}
	public Usuari getByUsername(String username) {

		String sql = "SELECT *"
				+ " FROM Usuari"
				+ " WHERE username='" + username + "'";
		
		return jdbcTemplate.queryForObject(sql, new UsuariMapper());
		
	}
	
	public void canviarContrasenya(Usuari usuari) {
		BasicPasswordEncryptor passwordEncryptor = new BasicPasswordEncryptor();
		String password = passwordEncryptor.encryptPassword(usuari.getPassword());
		String sql = "UPDATE Usuari set Password='" + password + "'"
				+ " WHERE username='" + usuari.getUsername() + "'";
		
		jdbcTemplate.update(sql);
		
	}

	


}