package naturadventure.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import naturadventure.dao.core.DaoInterface;
import naturadventure.dao.core.GenericDao;
import naturadventure.model.Categoria;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class CategoriaDao extends GenericDao<Categoria> implements DaoInterface<Categoria> {
	

	@Override
	public String getTable() {
		return "Categoria";
	}
	@Override
	public String[] getPrimaryKeys() {
		return new String[] {"nom"};
	}
	@Override
	public Object[] getPrimaryKeys(Categoria c) {
		return new Object[] {c.getNom()};
	}
	@Override
	public String[] getParams() {
		return new String[] {};
	}
	@Override
	public Map<String, Object> getParams(Categoria c) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		return params;
	}
	
	public List<String> getAllNoms(){
		String sql = "SELECT nom"
				+ " FROM Categoria;";
		return (List<String>) jdbcTemplate.queryForList(sql, String.class);
	}
	
	
	@Override
	public RowMapper<Categoria> getRowMapper() {
		return new CategoriaMapper();
	}
	
	private static final class CategoriaMapper implements RowMapper<Categoria> {

	    public Categoria mapRow(ResultSet rs, int rowNum) throws SQLException { 
	    	Categoria c = new Categoria();
			c.setNom(rs.getString("nom"));
			return c;
	    }
	}

}