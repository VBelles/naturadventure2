package naturadventure.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import naturadventure.dao.core.DaoInterface;
import naturadventure.dao.core.GenericDao;
import naturadventure.model.Activitat;
import naturadventure.model.TipusActivitat;
import naturadventure.model.ennumeration.HoraInici;
import naturadventure.model.ennumeration.Nivell;

import org.springframework.jdbc.core.RowMapper;


public class ActivitatDao extends GenericDao<Activitat> implements DaoInterface<Activitat> {


@Override
public String getTable() {
	return "Activitat";
}
@Override
public String[] getPrimaryKeys() {
	return new String[] {"codi"};
}
@Override
public Object[] getPrimaryKeys(Activitat a) {
	return new Object[] {a.getCodi()};
}
@Override
public String[] getParams() {
	return new String[] {"nom", "tipusActivitat", "horaInici", "duradaHores", "descripcio",
			"nivell", "preuPerPersona", "minParticipants", "maxParticipants"};
}
@Override
public Map<String, Object> getParams(Activitat a) {
	HashMap<String, Object> params = new HashMap<String, Object>();
	params.put("nom", a.getNom());
	params.put("tipusActivitat", a.getTipusActivitat());
	params.put("horaInici", a.getHoraInici());
	params.put("duradaHores", a.getDuradaHores());
	params.put("descripcio", a.getDescripcio());
	params.put("nivell", a.getNivell());
	params.put("preuPerPersona", a.getPreuPerPersona());
	params.put("minParticipants", a.getMinParticipants());
	params.put("maxParticipants", a.getMaxParticipants());
	return params;
}

@Override
public RowMapper<Activitat> getRowMapper() {
	return new ActivitatMapper();
}

private static final class ActivitatMapper implements RowMapper<Activitat> {

    public Activitat mapRow(ResultSet rs, int rowNum) throws SQLException { 
		Activitat a = new Activitat();
		a.setCodi(rs.getInt("codi"));
		a.setTipusActivitat(rs.getString("tipusActivitat"));
		a.setNom(rs.getString("nom"));
		a.setDuradaHores(rs.getInt("duradaHores"));
		a.setHoraInici(HoraInici.fromString(rs.getString("horaInici")));
		a.setDescripcio(rs.getString("descripcio"));
		a.setNivell(Nivell.fromString(rs.getString("nivell")));
		a.setPreuPerPersona(rs.getDouble("preuPerPersona"));
		a.setMinParticipants(rs.getInt("minParticipants"));
		a.setMaxParticipants(rs.getInt("maxParticipants"));
		return a;
    }
}


/*
 * 
 */


/** @return un mapa que cont� tota la informaci� continguda en les taules
 * Activitat i TipusActivitat de una determinada activitat. */
public Map<String, Object> getAllInfo(int codiActivitat) {
	String sql = "SELECT * FROM Activitat INNER JOIN TipusActivitat ON (Activitat.tipusActivitat = TipusActivitat.nom)"
			+ " WHERE Activitat.codi=" + codiActivitat;
	Map<String, Object> map = jdbcTemplate.queryForMap(sql);
	for(String key:map.keySet())
		System.out.println("key:" + key + " val:" + map.get(key));
	return map;
}



public List<Activitat> getByTipusActivitat(String tipusActivitat) {
	String sql = "SELECT * FROM Activitat WHERE tipusActivitat='" +tipusActivitat +"'";
	List<Activitat> activitats = jdbcTemplate.query(sql, getRowMapper());
	return activitats;
}

public List<Activitat> getByCategoria(String categoria) {
	String sql = "SELECT Activitat.* FROM Activitat INNER JOIN TipusActivitat ON (Activitat.tipusActivitat = TipusActivitat.nom)"
			+ " WHERE TipusActivitat.categoria='" + categoria+"'";
	List<Activitat> activitats = jdbcTemplate.query(sql, getRowMapper());
	return activitats;
}


/** @return el TipusActivitat de una determinada activitat. */
public TipusActivitat getTipusActivitat(int codiActivitat) {
	String sql = "SELECT TipusActivitat.nom, TipusActivitat.categoria, TipusActivitat.descripcio"
			+ " FROM Activitat INNER JOIN TipusActivitat ON (Activitat.tipusActivitat = TipusActivitat.nom)"
			+ " WHERE Activitat.codi=" + codiActivitat;
	return jdbcTemplate.queryForObject(sql, TipusActivitat.class);
}



/** Retorna una llista amb les ultimes activitats demanades.
 * TODO comprovar si funciona. */
public List<Activitat> getUltimesDemanades() {
	String sql = "SELECT *"
		   + " FROM (SELECT * FROM Activitat ORDER BY codi DESC LIMIT 30) AS ultimes"
		   + " ORDER BY codi ASC LIMIT 3";
	List<Activitat> ultimesDemanades = jdbcTemplate.query(sql, getRowMapper());
	return ultimesDemanades;
}


/** Retorna les activitats mes demanades.
 * TODO comprovar si funciona. */
public List<Activitat> getMesDemanades() {
	String sql = "SELECT * FROM Activitat WHERE codi IN("
			+ "SELECT codiActivitat FROM Reserva GROUP BY codiActivitat ORDER BY COUNT(codiActivitat) DESC LIMIT 3)";
	
	List<Activitat> mesDemanades = jdbcTemplate.query(sql, getRowMapper());
	return mesDemanades;
}


}