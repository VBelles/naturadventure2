package naturadventure.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import naturadventure.dao.core.DaoInterface;
import naturadventure.dao.core.GenericDao;
import naturadventure.model.Supervisio;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class SupervisioDao extends GenericDao<Supervisio> implements DaoInterface<Supervisio> {
	
	
	@Override
	public String getTable() {
		return "Supervisio";
	}
	@Override
	public String[] getPrimaryKeys() {
		return new String[] {"tipusActivitat", "usernameMonitor"};
	}
	@Override
	public Object[] getPrimaryKeys(Supervisio t) {
		return new Object[] {t.getTipusActivitat(), t.getUsernameMonitor()};
	}
	@Override
	public String[] getParams() {
		return new String[] {};
	}
	@Override
	public Map<String, Object> getParams(Supervisio t) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		return params;
	}

	
	@Override
	public RowMapper<Supervisio> getRowMapper() {
		return new SupervisioMapper();
	}
	
	private static final class SupervisioMapper implements RowMapper<Supervisio> {

	    public Supervisio mapRow(ResultSet rs, int rowNum) throws SQLException { 
	    	Supervisio s = new Supervisio();
	    	s.setTipusActivitat(rs.getString("tipusActivitat"));
			s.setUsernameMonitor(rs.getString("usernameMonitor"));
			return s;
	    }
	}

	//Aço es pot millorar
	public void update(String username, List<String> tipusActivitats) {
		System.out.println("Aci entra");
		String sqlRemove = "DELETE FROM Supervisio WHERE usernameMonitor='"+username+"'";
		jdbcTemplate.update(sqlRemove);
		for(String ta : tipusActivitats){
			add(new Supervisio(username, ta));
		}
		
		
	}


}