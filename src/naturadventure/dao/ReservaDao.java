package naturadventure.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import naturadventure.dao.core.DaoInterface;
import naturadventure.dao.core.GenericDao;
import naturadventure.model.Activitat;
import naturadventure.model.InfoPersonal;
import naturadventure.model.Reserva;
import naturadventure.model.ReservaActivitat;
import naturadventure.model.Usuari;
import naturadventure.model.ennumeration.Estat;
import naturadventure.model.ennumeration.HoraInici;
import naturadventure.model.ennumeration.Nivell;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class ReservaDao extends GenericDao<Reserva> implements DaoInterface<Reserva> {
	
	@Autowired
	ActivitatDao activitatDao;
	@Autowired
	InfoPersonalDao infoPersonalDao;
	@Autowired
	UsuariDao usuariDao;
	
	@Override
	public String getTable() {
		return "Reserva";
	}
	@Override
	public String[] getPrimaryKeys() {
		return new String[] {"codi"};
	}
	@Override
	public Object[] getPrimaryKeys(Reserva t) {
		return new Object[] {t.getCodi()};
	}
	@Override
	public String[] getParams() {
		return new String[] {"codiActivitat", "usernameMonitor", "codiInfo", "emailClient",
				"horaInici", "estat", "numParticipants", "preuFinal", "dataReserva", "dataActivitat"};
	}
	@Override
	public Map<String, Object> getParams(Reserva t) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("codiActivitat", t.getCodiActivitat());
		params.put("usernameMonitor", t.getUsernameMonitor());
		params.put("codiInfo", t.getCodiInfo());
		params.put("horaInici", t.getHoraInici());
		params.put("estat", t.getEstat());
		params.put("numParticipants", t.getNumParticipants());
		params.put("preuFinal", t.getPreuFinal());
		params.put("dataReserva", t.getDataReserva());
		params.put("dataActivitat", t.getDataActivitat());
		return params;
	}

	@Override
	public RowMapper<Reserva> getRowMapper() {
		return new ReservaMapper();
	}
	
	private static final class ReservaActivitatMapper implements RowMapper<ReservaActivitat> {

	    public ReservaActivitat mapRow(ResultSet rs, int rowNum) throws SQLException { 
	    	Reserva r = new Reserva();
	    	r.setCodi(rs.getInt("codi"));
			r.setCodiActivitat(rs.getInt("codiActivitat"));
			r.setUsernameMonitor(rs.getString("usernameMonitor"));
			r.setCodiInfo(rs.getInt("codiInfo"));
			r.setHoraInici(HoraInici.fromString(rs.getString("horaInici")));
			r.setEstat(Estat.fromString(rs.getString("estat")));
			r.setNumParticipants(rs.getInt("numParticipants"));
			r.setPreuFinal(rs.getDouble("preuFinal"));
			r.setDataReserva(rs.getDate("dataReserva"));
			r.setDataActivitat(rs.getDate("dataActivitat"));
			
			Activitat a = new Activitat();
			a.setCodi(rs.getInt("codi"));
			a.setTipusActivitat(rs.getString("tipusActivitat"));
			a.setNom(rs.getString("nom"));
			a.setDuradaHores(rs.getInt("duradaHores"));
			a.setHoraInici(HoraInici.fromString(rs.getString("horaInici")));
			a.setDescripcio(rs.getString("descripcio"));
			a.setNivell(Nivell.fromString(rs.getString("nivell")));
			a.setPreuPerPersona(rs.getDouble("preuPerPersona"));
			a.setMinParticipants(rs.getInt("minParticipants"));
			a.setMaxParticipants(rs.getInt("maxParticipants"));
			
			return new ReservaActivitat(a, r);

	    }
	}
	
	private static final class ReservaMapper implements RowMapper<Reserva> {

	    public Reserva mapRow(ResultSet rs, int rowNum) throws SQLException { 
	    	Reserva r = new Reserva();
	    	r.setCodi(rs.getInt("codi"));
			r.setCodiActivitat(rs.getInt("codiActivitat"));
			r.setUsernameMonitor(rs.getString("usernameMonitor"));
			r.setCodiInfo(rs.getInt("codiInfo"));
			r.setHoraInici(HoraInici.fromString(rs.getString("horaInici")));
			r.setEstat(Estat.fromString(rs.getString("estat")));
			r.setNumParticipants(rs.getInt("numParticipants"));
			r.setPreuFinal(rs.getDouble("preuFinal"));
			r.setDataReserva(rs.getDate("dataReserva"));
			r.setDataActivitat(rs.getDate("dataActivitat"));
			return r;
	    }
	}
	
	
	/*
	 * 
	 */

	
	/** @return la activitat que li correspon. */
	public Activitat getActivitat(int codiReserva) {
		String sql = "SELECT Activitat.*"
				+ " FROM Reserva INNER JOIN Activitat ON (Reserva.codiActivitat = Activitat.codi)"
				+ " WHERE Reserva.codi=" + codiReserva;
		return jdbcTemplate.queryForObject(sql, activitatDao.getRowMapper());
	}
	
	/** @return la infoPersonal que li correspon. */
	public InfoPersonal getInfoPersonal(int codiReserva) {
		String sql = "SELECT InfoPersonal.*"
				+ " FROM Reserva INNER JOIN InfoPersonal ON (Reserva.codiInfo = InfoPersonal.codi)"
				+ " WHERE Reserva.codi=" + codiReserva;
		return jdbcTemplate.queryForObject(sql, infoPersonalDao.getRowMapper());
	}
	
	/** @return el usuari (si hi ha) que li correspon. */
	public Usuari getUsuari(int codiReserva) {
		String sql = "SELECT Usuari.*"
				+ " FROM Reserva INNER JOIN InfoPersonal ON (Reserva.codiInfo = InfoPersonal.codi)"
				+ " FROM InfoPersonal INNER JOIN Usuari ON (InfoPersonal.codi = Usuari.codiInfo)"
				+ " WHERE Reserva.codi=" + codiReserva;
		return jdbcTemplate.queryForObject(sql, usuariDao.getRowMapper());
	}
	
	
	
	/** @return totes les reserves de un determinat estat. */
	public List<Reserva> getByEstat(Estat e) {
		String sql = "SELECT * FROM Reserva WHERE estat='" +e.toString() +"'";
		return jdbcTemplate.query(sql, getRowMapper());
	}
	
	/** @return totes les reserves de una determinada activitat. */
	public List<Reserva> getByActivitat(int codiActivitat) {
		String sql = "SELECT * FROM Reserva WHERE codiActivitat=" + codiActivitat;
		return jdbcTemplate.query(sql, getRowMapper());
	}
	
	/** @return totes les reserves de un determinat monitor. */
	public List<Reserva> getByMonitor(String usernameMonitor) {
		String sql = "SELECT * FROM Reserva WHERE usernameMonitor='" + usernameMonitor+"'";
		return jdbcTemplate.query(sql, getRowMapper());
	}
	
	public String getTipusActivitat(int codiReserva) {
		String sql = "SELECT Activitat.tipusActivitat "
				+ "FROM Reserva INNER JOIN Activitat ON (Reserva.codiActivitat = Activitat.codi) "
				+ "WHERE Reserva.codi=" + codiReserva;
		
		return jdbcTemplate.queryForObject(sql, String.class);
	}
	
	public List<ReservaActivitat> getReservaActivitat(Estat e){
		String sql = "SELECT Reserva.*, Activitat.* "
				+ "FROM Reserva INNER JOIN Activitat ON (Reserva.codiActivitat = Activitat.codi) "
				+ "WHERE estat='" +e.toString() +"'";
		return jdbcTemplate.query(sql, new ReservaActivitatMapper());
	}
	
	public List<ReservaActivitat> getReservesDelDia(String username, Calendar c) {

		String sql = "SELECT Reserva.*, Activitat.* "
				+ " FROM Reserva INNER JOIN Activitat ON (Reserva.codiActivitat = Activitat.codi) "
				+ " WHERE Reserva.usernameMonitor='" + username +"'"
		        + " AND EXTRACT(MONTH FROM Reserva.dataActivitat) = '"+c.get(Calendar.MONTH)+"'"
				+ " AND EXTRACT(YEAR FROM Reserva.dataActivitat) = '"+c.get(Calendar.YEAR)+"'"
				+ " AND EXTRACT(DAY FROM Reserva.dataActivitat) = '"+c.get(Calendar.DAY_OF_MONTH)+"'";
				
		return jdbcTemplate.query(sql, new ReservaActivitatMapper());
	}
	
	public List<Integer> getReservesDelMes(String username, Calendar c) {
		String sql = "SELECT EXTRACT(DAY FROM Reserva.dataActivitat)"
				+ " FROM Reserva"
				+ " WHERE Reserva.usernameMonitor='" + username +"'"
				+ " AND EXTRACT(YEAR FROM Reserva.dataActivitat) = '"+c.get(Calendar.YEAR)+"'"
				+ " AND EXTRACT(MONTH FROM Reserva.dataActivitat) = '"+c.get(Calendar.MONTH)+"'";
		
		
		return jdbcTemplate.queryForList(sql, Integer.class);
	}
	public List<ReservaActivitat> getReservesDelUsuari(String username) {
		String sql = "SELECT Reserva.*, Activitat.* "
				+ " FROM Reserva INNER JOIN Activitat ON (Reserva.codiActivitat = Activitat.codi) "
				+ " WHERE Reserva.usernameMonitor='" + username +"'";
		return jdbcTemplate.query(sql, new ReservaActivitatMapper());
	}
	
	public List<ReservaActivitat> getByCodiInfo(int codiInfo) {
		String sql = "SELECT Reserva.*, Activitat.* "
				+ " FROM Reserva INNER JOIN Activitat ON (Reserva.codiActivitat = Activitat.codi) "
				+ " WHERE Reserva.codiInfo=" + codiInfo +"";
		return jdbcTemplate.query(sql, new ReservaActivitatMapper());
	}

	
}