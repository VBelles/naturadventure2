package naturadventure.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import naturadventure.dao.core.DaoInterface;
import naturadventure.dao.core.GenericDao;
import naturadventure.model.TipusActivitat;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class TipusActivitatDao extends GenericDao<TipusActivitat> implements DaoInterface<TipusActivitat> {

	@Override
	public String getTable() {
		return "TipusActivitat";
	}
	@Override
	public String[] getPrimaryKeys() {
		return new String[] {"nom"};
	}
	@Override
	public Object[] getPrimaryKeys(TipusActivitat t) {
		return new Object[] {t.getNom()};
	}
	@Override
	public String[] getParams() {
		return new String[] {"categoria", "descripcio"};
	}
	@Override
	public Map<String, Object> getParams(TipusActivitat t) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("categoria", t.getCategoria());
		params.put("descripcio", t.getDescripcio());
		return params;
	}
	

	public List<String> getAllNoms(){
		String sql = "SELECT nom"
				+ " FROM TipusActivitat;";
		return (List<String>) jdbcTemplate.queryForList(sql, String.class);
	}
	
	public List<TipusActivitat> getByCategoria(String nomCategoria) {
		String sql = "SELECT * FROM TipusActivitat WHERE categoria='" +nomCategoria + "'";
		return jdbcTemplate.query(sql, getRowMapper());
	}


	@Override
	public RowMapper<TipusActivitat> getRowMapper() {
		return new TipusActivitatMapper();
	}
	
	private static final class TipusActivitatMapper implements RowMapper<TipusActivitat> {

	    public TipusActivitat mapRow(ResultSet rs, int rowNum) throws SQLException { 
	    	TipusActivitat t = new TipusActivitat();
	    	t.setNom(rs.getString("nom"));
	    	t.setCategoria(rs.getString("categoria"));
			t.setDescripcio(rs.getString("descripcio"));
			return t;
	    }
	}
}