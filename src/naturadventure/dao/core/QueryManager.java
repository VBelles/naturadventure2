package naturadventure.dao.core;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/** 
 * S'encarrega de construir les sent�ncies SQL
 * */
public class QueryManager {
	
	
	public static String selectAll(String table) {
		return "SELECT * FROM " + table;
	}
	
	
	/** Hacemos un select indicando la tabla en la que se realiza, pero sin indicar
	 * claves primarias. S� incluimos los campos que queremos que retorne la consulta. */
	public String select(String table, Set<String> params) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		
		for(String param:params) {
			sb.append(param).append(", ");
		}
		sb.delete(sb.length() - 2, sb.length());

		sb.append(" FROM ").append(table);
		
		return sb.toString();
	}
	
	
	/** Hacemos un select indicando la tabla en la que se realiza, y las claves primarias. */
	public static String select(String table, String[] primaryKeys, Object[] primaryKeysValues) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * ").append(" FROM ").append(table).append(" WHERE ");
		
		for(int i=0;i<primaryKeys.length;i++) {
			String primaryKey = primaryKeys[i];
			Object primaryKeyValue = primaryKeysValues[i];
			sb.append(primaryKey).append(" = ").append(quotes(primaryKeyValue));
			sb.append(" AND ");
		}
		sb.delete(sb.length() - 5, sb.length());
		
		return sb.toString();
	}
	
	
	/** Hacemos un select indicando la tabla en la que se realiza, las claves primarias
	 * con las que debe coincidir y los campos que queremos que retorne la consulta. 
	 * @param primaryKeysValues2 */
	public static String select(String table, String[] primaryKeys, Object[] primaryKeysValues, String[] params) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		
		for(String param:params) {
			sb.append(param).append(", ");
		}
		sb.delete(sb.length() - 2, sb.length());

		sb.append(" FROM ").append(table).append(" WHERE " );
		
		for(int i=0;i<primaryKeys.length;i++) {
			String primaryKey = primaryKeys[i];
			Object primaryKeyValue = primaryKeysValues[i];
			sb.append(primaryKey).append(" = ").append(quotes(primaryKeyValue));
			sb.append(" AND ");
		}
		sb.delete(sb.length() - 5, sb.length());

		return sb.toString();
	}
	
	
	/** Afegeix un objecte sense especificar claus prim�ries. */
	public static String add(String table, Map<String, Object> params) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO " + table + "(");
		
		for(String key:params.keySet()) {
			sb.append(key).append(", ");
		}
		sb.delete(sb.length() - 2, sb.length());
		sb.append(") VALUES(");
		for(String key:params.keySet()) {
			Object value = params.get(key);
			sb.append(quotes(value)).append(", ");
		}
		sb.delete(sb.length() - 2, sb.length());
		
		sb.append(") RETURNING codi");
		System.out.println(sb.toString());
		return sb.toString();
	}

	
	/** Afegeix un objecte a la taula amb les claus prim�ries especificades. */
	public static String add(String table, String[] primaryKeys, Object[] primaryKeysValues, Map<String, Object> params) {
		Map<String, Object> keysAndParams = new HashMap<String, Object>();
		for(int i=0;i<primaryKeys.length;i++)
			keysAndParams.put(primaryKeys[i], primaryKeysValues[i]);
		keysAndParams.putAll(params);
		
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO " + table + "(");
		
		for(String key:keysAndParams.keySet()) {
			sb.append(key).append(", ");
		}
		sb.delete(sb.length() - 2, sb.length());
		
		sb.append(") VALUES(");
		for(String key:keysAndParams.keySet()) {
			Object value = keysAndParams.get(key);
			if (value instanceof Number) {
				sb.append(value).append(", ");
			} else {
				sb.append("'").append(value).append("'").append(", ");
			}
			
		}
		sb.delete(sb.length() - 2, sb.length());
		
		sb.append(")");
		System.out.println(sb.toString());
		return sb.toString();
		
	}
	
	
	/** Actualitza un objecte. Tan sols accepta una clau prim�ria. */
	public static String update(String table, String primaryKey, Object primaryKeyValue, Map<String, Object> params) {
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE " + table + " SET ");
		
		for(String key:params.keySet()) {
			Object keyValue = params.get(key); 
			sb.append(key).append(" = ").append(quotes(keyValue)).append(", ");
		}
		sb.delete(sb.length() - 2, sb.length());
		
		sb.append(" WHERE " ).append(primaryKey).append(" = ").append(quotes(primaryKeyValue));

		return sb.toString();
	}
	
	
	/** Actualitza un objecte. Accepta mes d'una clau primaria. */
	public static String update(String table, String[] primaryKeys, Object[] primaryKeysValues, Map<String, Object> params) {
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE " + table + " SET ");
		
		for(String key:params.keySet()) {
			Object keyValue = params.get(key); 
			sb.append(key).append(" = ").append(quotes(keyValue)).append(", ");
		}
		sb.delete(sb.length() - 2, sb.length());
		
		sb.append(" WHERE " );
		
		for(int i=0;i<primaryKeys.length;i++) {
			String primaryKey = primaryKeys[i];
			Object primaryKeyValue = primaryKeysValues[i];
			sb.append(primaryKey).append(" = ").append(quotes(primaryKeyValue));
			sb.append(" AND ");
		}
		sb.delete(sb.length() - 5, sb.length());
		System.out.println(sb.toString());
		return(sb.toString());

	}
	
	
	public static String delete(String table, String primaryKey, Object keyValue) {
		return "DELETE FROM " + table + " WHERE " + primaryKey + " = '" + keyValue +"'";
	}
	
	
	public static String delete(String table, String[] primaryKeys, Object[] primaryKeysValues) {
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE FROM ").append(table).append(" WHERE ");
		
		for(int i=0;i<primaryKeys.length;i++) {
			String key = primaryKeys[i];
			Object keyValue = primaryKeysValues[i];
			sb.append(key).append(" = ").append(quotes(keyValue));
			sb.append(" AND ");
		}
		sb.delete(sb.length() - 5, sb.length());
		
		return sb.toString();

	}
	

	
	
	/** Depenent si un objecte es o no un numero, posa cometes alrededor seu.
	 * 
	 * S'usa en les clausules (WHERE paraula = 'String') o be (WHERE numero = 2)
	 * per asegurar que la sentencia sera correcta. */
	private static String quotes(Object o) {
		if (o == null) {
			return "null";
		}
		if (o instanceof Number)
			return o.toString();
		else
			return "'" + o.toString() + "'";
	}

}
