package naturadventure.dao.core;

import java.util.Map;

import org.springframework.jdbc.core.RowMapper;


/** 
 * Cal proporcionar el nom de la taula, els identificadors de les claus primàries,
 * i com aconseguir els seus valors al DAO perquè puga fer canvis en la BD.<br>
 * 
 * getParams() retorna tots aquells paràmetres que NO son claus primàries.<br>
 * 
 * buildObject() té en compte claus primàries i paràmetres.
 * */

public interface DaoInterface<T> {
		
	/** @return el nom de la taula. */
	String getTable();
	
	/** @return els identificadors de les claus prim�ries. */
	String[] getPrimaryKeys();
	
	/** @return els valors de les claus prim�ries. */
	Object[] getPrimaryKeys(T t);
	
	/** @return els identificadors dels altres par�metres. */
	String[] getParams();
	
	/** @return un mapa contenint els parells identificador-valor dels altres par�metres. */
	Map<String, Object> getParams(T t);
	
	/** @return el objecte que li permet a jdbcTemplate gestionar l'intercambi de dades. */
	RowMapper<T> getRowMapper();

}