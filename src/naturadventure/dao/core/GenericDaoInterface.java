package naturadventure.dao.core;

import java.util.Set;

public interface GenericDaoInterface<T> {

	/** Les operacions bàsiques dels DAO */

	/** Retorna un objecte donades les seues claus primàries. */
	T get(Object... primaryKeysValues);
	
	/** Retorna tots els objectes de la taula. */
	Set<T> getAll();
	
	/** Afegeix un objecte a la BD.
	 * @return el valor de la clau primària amb el cual s'ha inserit. */
	int add(T t);
	
	/** Actualitza un objecte a la BD.
	 * @return el nombre de updates que s'han realitzat. */
	int update(T t);
	
	/** Borra un objecte donades les seues claus primàries. */
	void delete(Object... primaryKeysValues);
	
}
