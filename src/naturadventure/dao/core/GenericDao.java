package naturadventure.dao.core;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public abstract class GenericDao<T> implements DaoInterface<T> {
	 
	protected JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
	    jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	public List<T> getAll() {
		return this.jdbcTemplate.query(QueryManager.selectAll(getTable()), getRowMapper());
	}
	
	public T get(Object... primaryKeysValues){
		return this.jdbcTemplate.queryForObject(QueryManager.select(getTable(), getPrimaryKeys(), primaryKeysValues), getRowMapper());
	}
	
	/** Afegeix l'objecte i especificant la clau prim�ria.
	 * @return 0 o 1 per indicar si l'objecte s'ha afegit. */
	public int add(T t) {
		return jdbcTemplate.update(QueryManager.add(getTable(), getPrimaryKeys(), getPrimaryKeys(t), getParams(t)));
	}
	
	/** http://stackoverflow.com/a/8645960
	 * Afegeix l'objecte prenent una clau prim�ria de manera autoincrementada.
	 * @return el codi amb el qual s'ha afegit. */
	public int addSerial(T t) {
		return jdbcTemplate.queryForObject(QueryManager.add(getTable(), getParams(t)), Integer.class);
	}
	
	public void update(T t) {
		this.jdbcTemplate.update(QueryManager.update(getTable(), getPrimaryKeys(), getPrimaryKeys(t), getParams(t)));	
	}
	
	public void delete(T t) {
		this.jdbcTemplate.update(QueryManager.delete(getTable(), getPrimaryKeys(), getPrimaryKeys(t)));
	}
	
	public void delete(Object... primaryKeysValues) {
		this.jdbcTemplate.update(QueryManager.delete(getTable(), getPrimaryKeys(), primaryKeysValues));
	}
	
}