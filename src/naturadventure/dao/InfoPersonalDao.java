package naturadventure.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import naturadventure.dao.core.DaoInterface;
import naturadventure.dao.core.GenericDao;
import naturadventure.model.InfoPersonal;
import naturadventure.model.Reserva;
import naturadventure.model.Usuari;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class InfoPersonalDao extends GenericDao<InfoPersonal> implements DaoInterface<InfoPersonal> {


	@Override
	public String getTable() {
		return "InfoPersonal";
	}
	@Override
	public String[] getPrimaryKeys() {
		return new String[] { "codi" };
	}
	@Override
	public Object[] getPrimaryKeys(InfoPersonal c) {
		return new Object[] { c.getCodi() };
	}
	@Override
	public String[] getParams() {
		return new String[] {"nom", "email", "telefon"};
	}
	@Override
	public Map<String, Object> getParams(InfoPersonal c) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("nom", c.getNom());
		params.put("email", c.getEmail());
		params.put("telefon", c.getTelefon());
		return params;
	}

	@Override
	public RowMapper<InfoPersonal> getRowMapper() {
		return new InfoPersonalMapper();
	}
	
	private static final class InfoPersonalMapper implements RowMapper<InfoPersonal> {

		public InfoPersonal mapRow(ResultSet rs, int rowNum) throws SQLException {
			InfoPersonal c = new InfoPersonal();
			c.setCodi(rs.getInt("codi"));
			c.setNom(rs.getString("nom"));
			c.setEmail(rs.getString("email"));
			c.setTelefon(rs.getString("telefon"));
			return c;
		}
	}
	
	
	/*
	 * 
	 */
	
	
	/** Retorna el usuari al cual correspon */
	public Usuari getUsuari(int codiInfo) {
		String sql = "SELECT * FROM Usuari WHERE Usuari.codiInfo=" + codiInfo;
		return jdbcTemplate.queryForObject(sql, Usuari.class);
	}
	
	/** Retorna les reserves que tenen assignada aquesta InfoPersonal. */
	public List<Reserva> getReserves(int codiInfo) {
		String sql = "SELECT * FROM Reserva WHERE Reserva.codiInfo=" + codiInfo;
		return jdbcTemplate.queryForList(sql, Reserva.class);
	}
	
}