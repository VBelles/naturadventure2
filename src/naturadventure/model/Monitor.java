package naturadventure.model;

import java.util.List;

import naturadventure.model.InfoPersonal;
import naturadventure.model.Usuari;

public class Monitor extends PersonaRegistrada {
	List<String> tipusActivitats;
	
	public Monitor(){
		super();
	}
	
	public Monitor(Usuari usuari, InfoPersonal infoPersonal) {
		super(usuari, infoPersonal);
	}



	public List<String> getTipusActivitats() {
		return tipusActivitats;
	}

	public void setTipusActivitats(List<String> tipusActivitats) {
		this.tipusActivitats = tipusActivitats;
	}
	

}
