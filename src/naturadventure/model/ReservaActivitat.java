package naturadventure.model;

public class ReservaActivitat {

	Activitat activitat;
	Reserva reserva;
	
	public ReservaActivitat(){}
	
	public ReservaActivitat(Activitat activitat, Reserva reserva) {
		this.activitat = activitat;
		this.reserva = reserva;
	}

	public Activitat getActivitat() {
		return activitat;
	}

	public void setActivitat(Activitat activitat) {
		this.activitat = activitat;
	}

	public Reserva getReserva() {
		return reserva;
	}

	public void setReserva(Reserva reserva) {
		this.reserva = reserva;
	}
}
