package naturadventure.model;

public class InfoPersonal {

	int codi;
	String nom;
	String email;
	String telefon;
	
	
	public int getCodi() {
		return codi;
	}
	public void setCodi(int codi) {
		this.codi = codi;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	
	public String toString() {
		return "InfoPersonal [" +
				"codi=" + codi +
				" nom=" + nom +
				" email=" + email +
				" telefon=" + telefon +
				"]";
	}

}