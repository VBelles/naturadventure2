package naturadventure.model;

import naturadventure.model.ennumeration.TipusUsuari;

public class Usuari {
	String username;
	String password;
	int codiInfo;
	TipusUsuari tipusUsuari;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getCodiInfo() {
		return codiInfo;
	}
	public void setCodiInfo(int codiInfo) {
		this.codiInfo = codiInfo;
	}
	public TipusUsuari getTipusUsuari() {
		return tipusUsuari;
	}
	public void setTipusUsuari(TipusUsuari tipusUsuari) {
		this.tipusUsuari = tipusUsuari;
	}

}
