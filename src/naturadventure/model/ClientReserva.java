package naturadventure.model;


public class ClientReserva {

	InfoPersonal infoPersonal;
	Reserva reserva;
	
	public ClientReserva(){}
	
	public ClientReserva(InfoPersonal infoPersonal, Reserva reserva) {
		this.infoPersonal = infoPersonal;
		this.reserva = reserva;
	}

	public InfoPersonal getInfoPersonal() {
		return infoPersonal;
	}

	public void setInfoPersonal(InfoPersonal infoPersonal) {
		this.infoPersonal = infoPersonal;
	}

	public Reserva getReserva() {
		return reserva;
	}

	public void setReserva(Reserva reserva) {
		this.reserva = reserva;
	}
	
}
