package naturadventure.model;

public class Supervisio {

	String tipusActivitat;
	String usernameMonitor;

	
	public Supervisio(String usernameMonitor, String tipusActivitat) {
		this.usernameMonitor = usernameMonitor;
		this.tipusActivitat = tipusActivitat;
	}
	public Supervisio(){}
	
	public String getTipusActivitat() {
		return tipusActivitat;
	}

	public void setTipusActivitat(String tipusActivitat) {
		this.tipusActivitat = tipusActivitat;
	}

	public String getUsernameMonitor() {
		return usernameMonitor;
	}

	public void setUsernameMonitor(String usernameMonitor) {
		this.usernameMonitor = usernameMonitor;
	}

}
