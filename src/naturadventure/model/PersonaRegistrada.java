package naturadventure.model;

import naturadventure.model.InfoPersonal;
import naturadventure.model.Usuari;

public class PersonaRegistrada {
	Usuari usuari;
	InfoPersonal infoPersonal;
	
	public PersonaRegistrada(){
	}
	
	public PersonaRegistrada(Usuari usuari, InfoPersonal infoPersonal) {
		this.usuari = usuari;
		this.infoPersonal = infoPersonal;
	}
	
	public void setUsuari(Usuari usuari) {
		this.usuari = usuari;
	}
	
	public Usuari getUsuari() {
		return usuari;
	}

	public void setInfoPersonal(InfoPersonal infoPersonal) {
		this.infoPersonal = infoPersonal;
	}
	
	public InfoPersonal getInfoPersonal() {
		return infoPersonal;
	}

}
