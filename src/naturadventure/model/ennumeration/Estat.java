package naturadventure.model.ennumeration;

public enum Estat {
	PENDENT("pendent"), ACCEPTADA("acceptada"), REBUTJADA("rebutjada");

	private Estat(String estat) {
		this.estat = estat;
	}

	private String estat;

	public static Estat fromString(String text) {
		if (text != null) {
			for (Estat s : Estat.values()) {
				if (text.equalsIgnoreCase(s.estat)) {
					return s;
				}
			}
		}
		throw new IllegalArgumentException("No existeix tal estat.");
	}

	public String toString() {
		return estat;
	}

}
