package naturadventure.model.ennumeration;

public enum HoraInici {
	MATI("mati"), VESPRADA("vesprada"), NIT("nit");

	private HoraInici(String hora) {
		this.hora = hora;
	}

	private String hora;

	public static HoraInici fromString(String text) {
		if (text != null) {
			for (HoraInici h : HoraInici.values()) {
				if (text.equalsIgnoreCase(h.hora)) {
					return h;
				}
			}
		}
		throw new IllegalArgumentException("No existe tal hora de inicio.");
	}

	public String toString() {
		return hora;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

}
