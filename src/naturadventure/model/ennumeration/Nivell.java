package naturadventure.model.ennumeration;

public enum Nivell {
	INICIACIO("iniciacio"), MITJA("mitja"), EXPERT("expert");

	private Nivell(String nivell) {
		this.nivell = nivell;
	}

	private String nivell;

	public static Nivell fromString(String text) {
		if (text != null) {
			for (Nivell n : Nivell.values()) {
				if (text.equalsIgnoreCase(n.nivell)) {
					return n;
				}
			}
		}
		throw new IllegalArgumentException("No existeix tal nivell.");
	}
	
	public String toString() {
		return nivell;
	}

	public String getNivell() {
		return nivell;
	}

	public void setNivell(String nivell) {
		this.nivell = nivell;
	}

}
