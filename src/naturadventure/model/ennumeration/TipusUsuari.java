package naturadventure.model.ennumeration;

public enum TipusUsuari {
	USUARI("usuari"), MONITOR("monitor"), ADMINISTRADOR("administrador");

	private TipusUsuari(String tipus) {
		this.tipus = tipus;
	}

	private String tipus;

	public static TipusUsuari fromString(String text) {
		if (text != null) {
			for (TipusUsuari h : TipusUsuari.values()) {
				if (text.equalsIgnoreCase(h.tipus)) {
					return h;
				}
			}
		}
		throw new IllegalArgumentException("No existeix tal tipus d'usuari.");
	}

	public String toString() {
		return tipus;
	}

}
