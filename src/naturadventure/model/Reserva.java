package naturadventure.model;

import java.util.Date;

import naturadventure.model.ennumeration.Estat;
import naturadventure.model.ennumeration.HoraInici;

public class Reserva {

	int codi;
	int codiActivitat;
	int codiInfo;
	String usernameMonitor;
	HoraInici horaInici;
	Estat estat;
	int numParticipants;
	double preuFinal;
	Date dataReserva;
	Date dataActivitat;
	
	
	public int getCodi() {
		return codi;
	}
	public void setCodi(int codi) {
		this.codi = codi;
	}
	public int getCodiActivitat() {
		return codiActivitat;
	}
	public void setCodiActivitat(int codiActivitat) {
		this.codiActivitat = codiActivitat;
	}
	public int getCodiInfo() {
		return codiInfo;
	}
	public void setCodiInfo(int codiInfo) {
		this.codiInfo = codiInfo;
	}
	public String getUsernameMonitor() {
		return usernameMonitor;
	}
	public void setUsernameMonitor(String usernameMonitor) {
		this.usernameMonitor = usernameMonitor;
	}
	public HoraInici getHoraInici() {
		return horaInici;
	}
	public void setHoraInici(HoraInici horaInici) {
		this.horaInici = horaInici;
	}
	public Estat getEstat() {
		return estat;
	}
	public void setEstat(Estat estat) {
		this.estat = estat;
	}
	public int getNumParticipants() {
		return numParticipants;
	}
	public void setNumParticipants(int numParticipants) {
		this.numParticipants = numParticipants;
	}
	public double getPreuFinal() {
		return preuFinal;
	}
	public void setPreuFinal(double preuFinal) {
		this.preuFinal = preuFinal;
	}
	public Date getDataReserva() {
		return dataReserva;
	}
	public void setDataReserva(Date dataReserva) {
		this.dataReserva = dataReserva;
	}
	public Date getDataActivitat() {
		return dataActivitat;
	}
	public void setDataActivitat(Date dataActivitat) {
		this.dataActivitat = dataActivitat;
	}
	
}