package naturadventure.model;

import naturadventure.model.ennumeration.HoraInici;
import naturadventure.model.ennumeration.Nivell;

public class Activitat {
	int codi;
	String tipusActivitat;
	String nom;
	int duradaHores;
	HoraInici horaInici;
	String descripcio;
	Nivell nivell;
	double preuPerPersona;
	int minParticipants;
	int maxParticipants;

	public int getCodi() {
		return codi;
	}

	public void setCodi(int codi) {
		this.codi = codi;
	}

	public String getTipusActivitat() {
		return tipusActivitat;
	}

	public void setTipusActivitat(String tipusActivitat) {
		this.tipusActivitat = tipusActivitat;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getDuradaHores() {
		return duradaHores;
	}

	public void setDuradaHores(int duradaHores) {
		this.duradaHores = duradaHores;
	}

	public HoraInici getHoraInici() {
		return horaInici;
	}

	public void setHoraInici(HoraInici horaInici) {
		this.horaInici = horaInici;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public Nivell getNivell() {
		return nivell;
	}

	public void setNivell(Nivell nivell) {
		this.nivell = nivell;
	}

	public double getPreuPerPersona() {
		return preuPerPersona;
	}

	public void setPreuPerPersona(double preuPerPersona) {
		this.preuPerPersona = preuPerPersona;
	}

	public int getMinParticipants() {
		return minParticipants;
	}

	public void setMinParticipants(int minParticipants) {
		this.minParticipants = minParticipants;
	}

	public int getMaxParticipants() {
		return maxParticipants;
	}

	public void setMaxParticipants(int maxParticipants) {
		this.maxParticipants = maxParticipants;
	}

	@Override
	public String toString() {
		return "Activitat [codi=" + codi + ", tipusActivitat="
				+ tipusActivitat + ", nom=" + nom + ", duradaHores="
				+ duradaHores + ", horaInici=" + horaInici + ", descripcio="
				+ descripcio + ", nivell=" + nivell + ", preuPerPersona="
				+ preuPerPersona + ", minParticipants=" + minParticipants
				+ ", maxParticipants=" + maxParticipants + "]";
	}
}